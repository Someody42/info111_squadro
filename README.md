Tutturu !

Quelques conventions de nommage :

- De manière générale, on utilise les majuscules pour séparer les mots : `monNomDeVariable`, `maFonction`, `MaClasse`
- les noms de fonctions et de variables commencent par une minuscule
- les noms de classes commencent par une Majuscule
- les attributs et méthodes privées/protégées commencent par un underscore
- les pointeurs commencent par `p_`
- les itérateurs commencent par `it_`
- les vector de pointeurs (i.e `std::vector<int*>`) commencent par `vp_` (à confirmer, parce que c'est vraiment laid xD)
- les attributs pointeurs commencent donc par `_p_`

Librairie à installer :
- `SFML` (Librairie graphique) : https://www.sfml-dev.org/

**ATTENTION** Nouvelle syntaxe pour lancer le programme

Syntaxe de lancement du programme (ne prends plus en compte l'ordre) : `./squadro attribut=valeur`\
Attributs : `menu`,`difficulty`, `interface`, `ai1`,`ai2`,`savePath`,`loadPath`
Paramètres synthétiques : `new` --> `savePath=None`

À ajouter : paramètres `ju` et `an` pour permettre, pendant le dev, de ne pas retaper toute une config à chaque fois

TODO
===

- Implémenter un tour de jeu : OK
- Implémenter l'interface en ligne de commandes : ~OK
  - Réfléchir à comment afficher les vitesses : exposants ? à côté ?
- Compléter la `SFMLInterface` : vitesses (OK), score, highlight
- Implémenter un système de "messages" entre le `GameCore`et l'interface ==> afficher infos à l'utilisateur : OK, à perfectionner
- Faire l'IA
- Éventuellement, réfléchir à un moyen de demander proprement à l'utilisateur les paramètres du jeu : OK

- Gérer les pions vainqueurs (conflits possibles avec le fichier texte de sortie)
- s'occuper des scores (fait, à vérifier)
- faire les tests de mouvement valide

- Dans l'interface en ligne de commandes, vérifier les valeurs rentrées par l'utilisateur
- Gérer proprement la destruction des pointeurs et les destructeurs

- Penser à comment optimiser les rotations de plateau

Quelques explications du code
===

Représentation du plateau de jeu
---
Comme on pourrait s'y attendre, le plateau du jeu, stocké dans l'attribut `_board` de la classe `GameCore`, est un tableau à deux dimensions (**Attention**, les lignes "du bord" font aussi partie du tableau, donc le plateau de base fait 7*7). Petite nouveauté cependant, le type des éléments de ce tableau : il s'agit d'une *énumération* appelée `CellState`, qui représente comme son nom l'indique l'état d'une case du plateau. Il n'y a pas grand chose de plus à savoir dessus, si ce n'est qu'une variable de ce type ne peut prendre que les valeurs écrites ci-dessous, qui sont en fait représentées par des caractères astucieusement choisis.

| Valeur          | Représentation en `char` | Description     |
|--------         |--------------------------|-------------    |
|`CellState::None`| `' '`                    |Une case vide    |
|`CellState::Up`  | `'^'`                    |Pion vers le haut|
|`CellState::Down`| `'v'`                    |Pion vers le bas |
|`CellState::Right`| `'>'`                   |Pion vers la droite|
|`CellState::Left`| `'<'`                    |Pion vers la gauche|

Représentation des vitesses des pièces
---
Le GameCore possède également un attribut `_speeds` qui contient les vitesses des différents pions. Son type est `std::pair<std::vector<int>,std::vector<int>>`, i.e c'est une paire de tableaux d'entiers. La paire est une structure de données qui ne contient que deux éléments (qui peuvent être de types différents), et qui sont stockés dans les attributs `first` et `second` de la paire.

Ici, `_speeds.first` désigne donc les vitesses à l'aller, et `_speeds.second` les vitesses au retour. **Attention**, pour la difficulté de base, `_board.size()==7` MAIS `_speeds.first.size()==_speeds.second.size()==5` !!!

Fonctionnement général
---

Pour faciliter la compréhension, voilà un petit résumé de qui appelle quoi, comment et quand pendant le déroulé d'une partie :

Tout commence dans le `main`, qui est, comment dire, très simple. La première ligne importante est :

```c++
AbstractGameMenu* p_gameMenu = AbstractGameMenu::getGameMenu(argc,argv);
```

On appelle donc la fonction `AbstractGameMenu::getGameMenu(int, char**)`. Nom bizarre hein ? En fait, si tu te souviens bien, ça doit te faire penser à comment on écrit une méthode. Sauf que là, on n'a pas créé d'objet `AbstractGameMenu`, donc comment on peut appeler une méthode ? Eh bien en fait, il s'agit de ce qu'on appelle une méthode **statique**. En fait, c'est une fonction normale, mais comme elle est destinée à avoir un rôle important avec la classe `AbstractGameMenu`, on la regroupe avec le reste.

Ok, alors, que fait cette fonction ? Globalement, elle a deux rôles principaux : d'abord, elle va lire les paramètres du programmes (`argc` et `argv`) et l'utiliser pour créer une `map` des arguments.

Questions diverses à poser aux profs
===

- À quel point faut-il connaître le code de l'autre ?
- Comment tester une classe ?
- Est-on noté sur la "beauté" de l'interface ?
- Spécifications de l'IA (format de lecture, etc...)
- Comment/Peut-on *facilement* hériter d'un flux ?

Intro à la POO
===
Jusqu'ici, en C++, on ne sait manipuler que des choses assez basiques :
- Des booléens
- Des entiers
- Des caractères (qui sont en fait un type particulier d'entiers)
- Des chaînes de caractère (qui sont en fait des tableaux de caractères)

Mais, dans un programme plus important, on va vite avoir besoin de stocker des structures plus complexes que ça. On a déjà vu un exemple avec les tableaux, qui permettent d'assembler un grand nombre de données **du même type** (un point important, c'est que la restriction du type n'est pas seulement liée à des soucis techniques, mais aussi à un choix sémantique : en théorie, dans un tableau, tous les éléments ont un rôle équivalent, ils ont juste été rassemblés dans une collection).

Maintenant, supposons qu'on veuille représenter une structure plus complexe, par exemple un personnage, par une variable `perso`. Notre personnage, on peut le caractériser par trois choses : son nom (`string`), ses points de vie (`ìnt`) et son attaque (`int`). On voit bien qu'aucun type du C++ ne correspond à notre personnage, pas même un tableau : notre personnage n'est pas une variable "de base", c'est un **objet**.  

Et ça tombe bien, le C++ est ce qu'on appelle un langage **orienté objet** : nous allons donc pouvoir manipuler notre objet comme toute autre variable. Oui, mais il y a un problème : en C++, toute variable doit avoir un type. Et le type `Personnage`, C++ le connaît pas. Il va donc falloir le créér : on dit que l'on va écrire une **classe** `Personnage`. (Attention : il faut bien faire la distinction entre la classe, c'est à dire le type, et l'objet, c'est à dire la variable)  

En C++, on ferait comme ça :

```c++
//Création de la classe
class Personnage {
  std::string _nom;
  int _attaque;
  int _pointsDeVie;
};

//Création d'un objet = INSTANCIATION
Personnage perso;

//Manipulation
perso._nom = "Toto";
perso._attaque = 1;
perso._pointsDeVie = 100;

std::cout<<perso._nom<<std::endl; //Affiche "Toto"
```
___
On notera le `;` qui fait chier à la fin de la déclaration de la classe (cherche pas, ya pas de logique).

Un peu de vocabulaire en passant, les "sous-variables" `_nom`, `attaque` et `_pointsDeVie` sont appelés des **attributs**.

Voilà, là on est content, tout marche bien, c'est la joie, etc... Sauf que, si on montre ce code à quelqu'un qui fait du C++ de manière un peu poussée, il risque de faire une crise cardiaque. Pourquoi ? Parce qu'il brise un principe fondamental : l'**encapsulation**.

Le principe d'encapsulation
---

Le principe d'encapsulation, c'est le genre d'idées qui paraissent d'abord être des restrictions inutiles et plutôt génantes, mais qui finissent par se révéler vraiment pratiques (si si je te jure). Voilà donc ce principe d'encapsulation :

*On ne doit pas pouvoir accéder directement aux attributs d'un objet depuis l'extérieur*

Pour voir l'intérêt de cette restriction, il faut penser en grand. Dans un gros programme, on aura sans problèmes des centaines de classes avec des milliers d'objets qui s'échangent des informations en continu. Si tous ces échanges se faisaient en modifiant directement les attributs de chaque objet, ça voudrait dire que le programmeur de chaque classe devrait connaître parfaitement le fonctionnement interne de tous les objets avec lesquels sa classe va communiquer. Après tout, qu'est-ce qui nous garantit que `perso._nom` contient le nom du personnage, et pas le chemin du fichier où est stocké ce nom ?  
Autre exemple : les points de vie de notre personnage, ils peuvent être modifiés par BEAUCOUP de fonctions un peu par tout dans le code ! Du coup, à chaque fois qu'une fonction veut modifier l'attribut `_pointsDeVie` d'un objet, il faudrait qu'elle vérifie qu la nouvelle vie du personnage ne dépasse pas la vie maximale, qu'elle le tue si jamais les points de vie passent à 0, etc... Plein de vérification qu'on aurait bien laissé au développeur de la classe `Personnage` qui, lui, sait comment elle marche.

Et bien c'est justement l'idée de l'encapsulation : quelqu'un qui utilise ou communique avec une classe n'a pas besoin de connaître son fonctionnement interne, et ne devrait en aucun cas aller modifier des attributs qui pourraient empêcher ce fonctionnement interne.

Du coup c'est décidé, pas de modification des composants internes de l'objet. Pour spécifier ça, en C++, on met le mot clé `private:` avant la déclaration des attributs. Notre classe devient alors :

```c++
//Création de la classe
class Personnage {
private:
  std::string _nom;
  int _attaque;
  int _pointsDeVie;
};
```
___

Oui mais, du coup on a un problème : jusqu'à présent, notre seul moyen d'interagir avec notre objet, c'était de lire/modifier ses attributs, mais on a plus le droit de le faire. Il faut donc un autre moyen d'interagir avec cet objet, sans avoir besoin d'aller toucher à son fonctionnement interne : on a appelé ça les **méthodes**.

Les méthodes
---

On l'a dit le principe d'encapsulation nous interdit de modifier les attributs (privés) d'un objet depuis l'extérieur. Mais si on peut pas le faire depuis l'extérieur, ça sous-entend qu'on peut le modifier de l'intérieur. Pour faire cela, on va créer des fonctions un peu particulières, appelées des **méthodes** ou encore **fonctions membres**. Ce seront elles qui feront le lien entre l'utilisateur et les attributs privés de l'objet. Comme elles sont accessibles de l'extérieur, les méthodes sont (en général) **publiques**. Prenons l'exemple d'une méthode `prendreDegats(int n)` qui retire `n` points de vie à l'objet. Le code correspondant serait le suivant :

```c++
//Création de la classe
class Personnage {
//Attributs privés
private:
  std::string _nom;
  int _attaque;
  int _pointsDeVie;
//Méthodes publiques
public:
  void prendreDegats(int n) {
    _pointsDeVie = max(0,_pointsDeVie-n);
  }
};

//Création d'un objet
Personnage perso;

//Utilisation de la méthode prendreDegats
perso.prendreDegats(10);
```
___
Plusieurs choses à voir dans ce code. D'abord, il ne marche pas, mais nous verrons cet aspect en dernier.

- Premièrement, on voit que les méthodes peuvent utiliser les variables privées de l'objet comme elles le veulent, sans avoir à les déclarer, puisqu'elles existent déjà dans l'objet
- Deuxièmement, l'appel d'une méthode est toujours lié à l'objet sur lequel elle agit. Pour rappel, la classe est un type, un objet est une variable, donc on peut très bien avoir des milliers de personnage. Appeler juste la méthode sans préciser sur quel personnage elle doit agir n'aurait donc aucun sens.

Après viennent les problèmes : ce code ne marche pas pour une raison très simple : nous avons oublié d'**initialiser** notre objet. Et oui, un objet est une variable, il faut donc l'initialiser. Et initialiser un objet, c'est initialiser chacun de ces attributs. Mais là, on a un problème : le principe d'encapsulation nous interdit de modifier directement les attributs. Le seul moyen de le faire, ce serait que l'objet s'initialise de l'intérieur, avec une méthode par exemple. Ça tombe bien, les créateurs du C++ ont eu la même idée, et ils ont donc introduit quelques méthodes particulières.

Les méthodes particulières : Constructeurs et Destructeurs
---

La première concerne directement notre problème d'initialisation : il s'agit du **constructeur** qui, comme son nom l'indique, est appelé à la construction de l'objet, et est donc chargé de l'initialiser. Pour notre classe `Personnage` un constructeur possible serait le suivant :

```c++
//Création de la classe
class Personnage {
//Attributs privés
private:
  ...
//Méthodes publiques
public:
  void prendreDegats(int n) {...}

  //Constructeur
  Personnage() {
    _nom = "Albert";
    _attaque = 10;
    _pointsDeVie = 100;
  }
};

//Création d'un objet
Personnage perso();
```
___

Un petit point syntaxe s'impose :
- Un constructeur a le même nom que le type qu'il construit, ici `Personnage`
- Un constructeur **n'a pas de type de retour**, même pas `void`, et il en sera de même pour les destructeurs
- On notera que j'ai rajouté des `()` à la fin de la création de mon objet, afin de mettre en valeur que c'est à ce moment que se fait l'appel au constructeur. En réalité, elles ne sont pas utiles ici, car lors de la création d'un objet, le constructeur sans paramètre est toujours appelé (et C++ le crée si besoin). Un tel constructeur sans paramètre est appelé constructeur **par défaut**.
- Une autre écriture possible (elle n'est pas rigoureusement équivalente mais on va faire comme si) serait `Personnage perso = Personnage();`

Bon, c'est très bien, notre objet est initialisé donc on peut l'utiliser, mais il reste un problème : tous nos personnages vont s'appeler Albert, avoir 100 HP et une attaque de 10 (sauf si on le change par la suite). On ne peut pas initialiser à autre chose ? Si, mais pour ça, il faut créer un autre constructeur, qui cette fois prendra des paramètres. Ça donnerait ça :

```c++
//Création de la classe
class Personnage {
//Attributs privés
private:
  ...
//Méthodes publiques
public:
  void prendreDegats(int n) {...}

  //Constructeur par défaut
  Personnage() {
    _nom = "Albert";
    _attaque = 10;
    _pointsDeVie = 100;
  }

  //Constructeur dit "surchargé"
  Personnage(std::string nom, int pointsDeVie) {
    _nom = nom;
    _pointsDeVie = pointsDeVie;
    _attaque = pointsDeVie/10;
  }
};

//Création d'un objet
Personnage perso("Toto",10);
```
___

On peut donc bien changer le nom et les points de vie de notre personnage. Pourquoi pas l'attaque ? Parce que le développeur de la classe, c'est à dire moi, a décidé qu'un personnage aurait toujours une attaque correspondant à 10% de sa vie. J'ai fait ça pour montrer que l'idée de constructeur n'est pas juste un moyen de rendre quelque chose inutilement complexe : de cette manière, quelqu'un qui utilise cette classe personnage n'a pas à ce soucier de comment initialiser son attaque : c'est un attribut privé, seul l'objet et ses méthodes ont à s'en occuper.

Voilà donc la base de ce qu'est un constructeur. Cependant, il y a en C++ une autre méthode "spéciale" qui fait l'effet inverse : il s'agit du destructeur, appelé quand il faut supprimer la variable. La syntaxe du destructeur est la même que celle du constructeur, mais avec un `~` devant : `~Personnage() {...}`. La plupart du temps, on a pas besoin de s'en occuper : C++ en construit un par défaut qui supprime tout ce qu'il faut, et il n'y a pas de problème. Mais il y a des cas où il faut le faire soi-même :
- quand un fichier a été ouvert, il faut, si ce n'est pas déjà fait, le fermer dans le destructeur, pour éviter qu'il reste ouvert jusqu'à la fin du programme
- quand on a utilisé `new` pour créer un pointeur par allocation dynamique (voir tuto pointeurs), il faut libérer la mémoire avec `free` dans le destructeur.

Voilà donc un tour d'horizon des méthodes spéciales en C++.

Accesseurs
---

Un petit peu de vocabulaire : on appelle communément **accesseurs** les méthodes d'un objet qui permettent juste de lire/modifier la valeur d'un attribut. En réalité ce sont des méthodes tout à fait classiques, mais on leur donne un nom particulier pour les identifier et les séparer des fonctions qui servent vraiment à faire quelque chose. La convention qu'on utilisera consiste à associer à l'attribut `type _monAttribut` les méthodes `type getMonAttribut()` pour lire et `void setMonAttribut(type valeur)` pour modifier. Ça paraît un peu contre-productif de créer comme ça plein de fonctions alors que sans ce p***** de principe d'encapsulation on aurait pu le faire directement, mais ça permet à l'objet de faire plein de vérifications pour vérifier que l'utilisateur n'est pas en train de détruire sa structure interne.

Classes et Programmation modulaire
---

Pour des raisons que le prof d'info expliquera sûrement bien mieux que moi jeudi, on aime bien séparer les fonctions dans deux fichiers différents : le prototype dans le fichier `.hpp` et le code en lui-même dans le fichier `.cpp`. Il en va de même pour les classes, et voilà ce que ça donnerait  pour notre classe `Personnage`, avec deux accesseurs en plus :

- Fichier `Personnage.hpp`

```c++
class Personnage {
//Attributs privés
private:
  std::string _nom;
  int _attaque;
  int _pointsDeVie;
//Méthodes publiques
public:
  void prendreDegats(int n);

  Personnage();
  Personnage(std::string nom, int pointsDeVie);

  std::string getNom();
  void setNom(std::string nouveauNom);
};
```
___
- Fichier `Personnage.cpp`

```c++
#include "Personnage.hpp"

void Personnage::prendreDegats(int n) {
  _pointsDeVie = max(0,_pointsDeVie-n);
}

Personnage::Personnage() {
  _nom = "Albert";
  _pointsDeVie = 100;
  _attaque = 10;
}

Personnage::Personnage(std::string nom, int pointsDeVie) {
  _nom = nom;
  _pointsDeVie = pointsDeVie;
  _attaque = pointsDeVie/10;
}

std::string Personnage::getNom() {
  return _nom;
}

void Personnage::setNom(std::string nouveauNom) {
  //Quelques vérifications...
  _nom = nouveauNom;
}
```
___
Le point important ici est l'utilisation de `::`, appelé opérateur de résolution de portée, pour préciser qu'on ne parle pas de n'importe qu'elle fonction mais bien d'une méthode, de la classe `Personnage`

Voilà donc la fin de la partie "facile" de la Programmation Orientée Objet. Il ne reste que deux *très* gros morceaux : l'héritage et le polymorphisme.

Intro aux pointeurs
==

Soit le code : `int a = 5;`  
Comme toute variable, `a` comporte :
- une adresse : `&a`
- un type : `int`
- une valeur : `5`

Les pointeurs sont aussi des variables, mais avec une particularité : leur valeur est **l'adresse** d'une autre variable.

Pour créer un *pointeur sur `a`*, qui sera donc une nouvelle variable `p_a`, il faut faire :
`int* p_a = &a;`  
Comme toute variable, `p_a` comporte :
- une adresse : `&p_a` à ne pas confondre avec `&a`
- un type : `int*` = *pointeur sur `int`*
- une valeur : `&a`

Pour retrouver la valeur de `a`, il faut alors faire l'opération suivante, appelée **déréférencement** : `*p_a`

Bon, là normalement c'est le moment où on se dit que c'est vraiment juste une manière plus compliquée de faire la même chose. En fait, pas vraiment, parce que ça change des choses assez importantes.

Passage par valeur ou par référence
--
Par défaut, en C++, les variables sont passées par **valeur**, c'est-à-dire qu'après l'instruction `b=a`, `b` contient la même valeur que `a`, mais `b` n'est pas `a` : si on modifie l'une, l'autre reste inchangée.
Il en va de même lors du passage d'arguments à une fonction, comme dans le code suivant :
~~~~c++
void f(int x){
  x++;
}

int main() {
  int a = 0;
  f(a);
  std::cout<<a<<std::endl;
}
~~~~

Ce code affichera zéro : ce n'est pas `a` qui a changé mais une autre variable `x` qui a été initialisée à la même valeur que `a` (notons que le résultat aurait été le même si le paramètre formel de la fonction s'était appelé `a` également !).

Le principal avantage de ce choix est qu'il évite les effets de bords incontrôlés, puisqu'une fonction n'a a priori pas d'influence sur les variables déclarées ailleurs dans le code.

Cependant, dans certains cas, ce comportement ne nous convient pas. C'est notamment le cas quand on veut effectivement qu'une fonction agisse sur une variable externe. Une possibilité, dans ce cas là, est d'utiliser les pointeurs. Le code précédent donnerait alors :
~~~~c++
void f(int* p_x){
  (*p_x)++;
}

int main() {
  int a = 0;
  f(&a);
  std::cout<<a<<std::endl;
}
~~~~
Et ce code affichera 1 ! Il faut bien comprendre la différence : dans un cas, on appelle la fonction en lui donnant la valeur de `a`, qu'elle va copier dans une nouvelle variable pour ensuite travailler dessus. Dans le deuxième cas, on appelle la fonction en lui donnant *l'adresse* de `a`, puis elle copie cette adresse en la stockant dans un pointeur `p_x`, et en déréférençant ce pointeur, elle va retrouver la véritable variable `a`, et non plus une copie. Dans le deuxième cas, on dit qu'on a fait un passage par référence (en réalité c'est un peu un abus de langage, car le terme "référence" renvoie encore à un autre objet en C++).

Globalement, cette idée de pouvoir passer à une autre fonction un moyen de pouvoir accéder directement à une variable qui est gérée par une autre fonction, c'est un des points fort des pointeurs. En effet, ça permet à plusieurs bouts du programme de se partager et d'interagir avec une même variable. Sauf que, pour le moment, la seule manière qu'on connaît pour créer un pointeur, c'est à partir d'une variable "normale". Or, ces variables ont un gros défaut : elles sont supprimées à la fin d'une fonction. Par exemple, on ne peut pas faire :

```c++
int* initialisePointeur(){
  int a = 5;
  return &a;
}

int main(){
  int* p_a = initialisePointeur();
  std::cout<<*p_a<<std::endl;
}
```
Ce code résultera probablement en une segmentation fault (en pratique il marchera dans certains cas). En effet, la fonction initialisePointeur renvoie un pointeur sur sa variable locale `a`, mais celle ci est supprimée dès qu'on sort de la fonction. On se retrouve donc avec un pointeur vers une case mémoire qui ne nous appartient plus ! Pas très pratique pour se partager des trucs... Pour pouvoir éviter ce problème, il va nous falloir un moyen de créer un pointeur sans passer par une variable "classique".

L'allocation dynamique (et ses dangers...)
---

Pour faire ça, il pourrait être tentant de faire comme pour une variable quelconque, ce qui donne quelque chose comme ça :
```c++
int* p_a; //Déclaration
*p_a = 5; //Initialisation
```

Mais, encore une fois, ce code est une source à `SEGFAULT`. En effet, ici, ce n'est pas vraiment le pointeur que nous avons initialisé, mais la case mémoire sur lequel il pointe. Lui, il contient une adresse quelconque, probablement utilisée par un autre programme, et on est en train d'essayer d'écrire à cette adresse !

Mais alors, notre pointeur, on le fait pointer sur quoi ? On a dit que ça ne pouvait pas être vers une variable créée par le langage C++, puisqu'elle sera probablement supprimée. Du coup, on va aller demander directement au grand maître : le système d'exploitation. La discussion se passe en gros comme ça :

- Programme : Salut j'ai besoin de mémoire
- Système : Ok, je t'en mets combien ?
- Programme : Bof, je sais pas, de quoi stocker un entier quoi
- Système : Ok, je t'ai créé une place dans ma mémoire. Tu la trouveras à l'adresse `0x57777...`. Ne l'oublie pas surtout !
- Programme : Ok, je garde ça précieusement !
- Système : Et, ma mémoire, elle s'appelle "reviens" !

En version un peu moins poétique (mais plus concise), ça donne ça :
```c++
int* p_a = new int;
```

Voilà, tu viens de voir ta première **allocation dynamique** de mémoire ! Et là, c'est le début des problèmes. Mets toi bien dans la tête que tu viens de découvrir la ligne de code qui cause le plus de problèmes aux programmeurs, celle qui ralentit un programme, celle qui surcharge la RAM, celle qui fait que tu dois redémarrer ton PC tous les trois jours...

En effet, rappelle toi pourquoi on a fait ça : on ne voulait pas que le langage s'occupe de nos affaires. Mais, du coup, on doit tout gérer comme des grands. En particulier, c'est à nous d'aller rendre sa mémoire au système quand on n'en a plus besoin. Le code correct serait donc plutôt :

```c++
int* p_a = new int;
//Utilisation...
delete p_a;
```

Et si on ne le fait pas, qu'est-ce qui se passe ? Eh bien, le système va continuer à ce sire que cette case mémoire est utilisée par notre programme, et ce même si notre programme s'est arrêté depuis des années ! C'est ce qu'on appelle une **fuite de mémoire**. Si ça concerne juste un octet perdu quelque part, c'est pas bien grave. Le problème, c'est que ça s'accumule, et que le seul moyen de pouvoir réutiliser cette mémoire, c'est de redémarrer l’entièreté du système !!!!

Les pointeurs et la POO
===

Autres concepts utiles
===

Les `switch`
---
Le `switch` est une structure de contrôle destinée à un usage très particulier. Comme la boucle `do while`, il n'introduit aucune nouvelle mécanique, mais il a l'avantage de simplifier l'écriture mais aussi la lecture du code. La sémantique est la suivante : on veut exécuter des instructions en fonction de la valeur d'une variable, et il est particulièrement bienvenu lorsqu'il y a plein de cas possibles à traiter.

Il y a quelques restrictions à bien connaître pour bien l'utiliser :
- le type de la variable étudiée doit être un type **intégral**, c'est à dire qu'il est représenté dans la mémoire comme un entier. Cela inclut évidemment les `int` et leurs versions `long`, `short` et `unsigned`, mais aussi les `char` (!!) et les `bool`, ainsi que toutes les énumérations (qui doivent en fait être déclarées à partir d'un type intégral)
- **on ne peut tester que l'égalité** : si je veux exécuter du code pour tous les entiers plus petits que 10000, je devrais les écrire un par un
- on ne peut tester l'égalité qu'avec **des valeurs connues à la compilation**, donc pas d'autres variables

Pour finir, voilà un exemple :

```c++
switch (maVariable) {
  case 1:
    instruction1;
  case 2:
    instruction2;
    break;
  case 3:
  case 4:
    instruction3;
    break;
  default:
    instruction_defaut;
    break;
}
```

Bon, je te l'accorde, c'est incompréhensible. Pourquoi des fois ya des `break`, pourquoi des fois non, pourquoi il y a deux `case` à la suite...

Le fonctionnement est le suivant : dès qu'il voit un case qui correspond à la valeur de `maVariable`, le programme rentre dans le code des `case`, et **il n'en sortira qu'en rencontrant une instruction `break`** ou en sortant du `switch`. Autrement dit, si `maVariable` vaut 1, le code ci-dessus exécutera les instructions 1 **puis** 2. On note alors que l'ordre est important, puisque si `maVariable` vaut 2, seule l'instruction 2 sera utilisée.

La même logique permet de comprendre "l'empilement" de `case`. On l'a dit, une fois qu'une condition est vraie, le programme continue d'exécuter les instructions jusqu'au prochain `break` sans se soucier des autres tests. Ce code permet donc de dire que l'on veut exécuter l'instruction 3 si et seulement si `maVariable` vaut 3 ou 4.

Enfin, quand il rencontre `default`, le programme commence a exécuter les instructions quelle que soit la valeur de `maVariable`. Attention cependant, le `break` fait complètement sortir du `switch`, même si la valeur de `maVariable` apparaissait une deuxième fois dans un autre case plus bas. Dans ce cas, même le code du `default` ne sera pas exécuté.
