#FROM WIKIPEDIA xD

# Generic GNUMakefile

# Just a snippet to stop executing under other make(1) commands
# that won't understand these lines
ifneq (,)
This makefile requires GNU Make.
endif

#PROGRAM = squadro
LIBSQUADRO_EXTENDED_CPPS := $(shell find libsquadro/ -type f -name '*.cpp')
LIBSQUADRO_EXTENDED_OBJS := $(patsubst %.cpp, %.o, $(LIBSQUADRO_EXTENDED_CPPS))
LIBSQUADRO_AIS_CPPS := $(shell find libsquadro/AIs -type f -name '*.cpp')
LIBSQUADRO_AIS_OBJS := $(patsubst %.cpp, %.o, $(LIBSQUADRO_AIS_CPPS))
LIBSQUADRO_CORE_CPPS := libsquadro/GameCore.cpp libsquadro/utilities.cpp libsquadro/Interfaces/AbstractInterface.cpp libsquadro/Interfaces/CLIInterface.cpp libsquadro/AIs/AbstractAI.cpp
LIBSQUADRO_CORE_OBJS := $(patsubst %.cpp, %.o, $(LIBSQUADRO_CORE_CPPS))
CC = g++
CFLAGS = -std=c++17 -O3
LDFLAGS =
LDLIBS = -lm -lsfml-graphics -lsfml-window -lsfml-system -lncurses -lstdc++ -lstdc++fs

all: game AI train tests tournoi naive_AI minimal

minimal: .depend Subprograms/minimal.o $(LIBSQUADRO_CORE_OBJS) libsquadro/AIs/ScoreEvaluators.o libsquadro/AIs/ScoreMaximizingAI.o
	$(CC) $(CFLAGS) Subprograms/minimal.o $(LIBSQUADRO_CORE_OBJS) libsquadro/AIs/ScoreEvaluators.o libsquadro/AIs/ScoreMaximizingAI.o $(LDFLAGS) -o Subprograms/minimal -lm -lstdc++

debug:
	make -f makefile-debug

game: .depend main.o $(LIBSQUADRO_EXTENDED_OBJS)
	$(CC) $(CFLAGS) main.o $(LIBSQUADRO_EXTENDED_OBJS) $(LDFLAGS) -o squadro $(LDLIBS) `pkg-config gtkmm-3.0 --libs`

tests: .depend Subprograms/Tests.o $(LIBSQUADRO_EXTENDED_OBJS)
	$(CC) $(CFLAGS) Subprograms/Tests.o $(LIBSQUADRO_EXTENDED_OBJS) $(LDFLAGS) -o Subprograms/Tests $(LDLIBS) `pkg-config gtkmm-3.0 --libs`

tournoi: .depend Subprograms/tournoi.o Subprograms/jeu_simulation.o $(LIBSQUADRO_CORE_OBJS) Subprograms/ExternalAI.o
	$(CC) $(CFLAGS) Subprograms/jeu_simulation.o $(LIBSQUADRO_CORE_OBJS) Subprograms/ExternalAI.o $(LDFLAGS) -o Subprograms/jeu_simulation -lm -lstdc++ -lstdc++fs
	$(CC) $(CFLAGS) Subprograms/tournoi.o $(LIBSQUADRO_CORE_OBJS) Subprograms/ExternalAI.o $(LDFLAGS) -o Subprograms/tournoi -lm -lstdc++ -lstdc++fs

AI: .depend Subprograms/Dedecker_Berry_IA.o  libsquadro/AIs/ScoreMaximizingAI.o libsquadro/AIs/ScoreEvaluators.o $(LIBSQUADRO_CORE_OBJS)
	$(CC) $(CFLAGS) Subprograms/Dedecker_Berry_IA.o  libsquadro/AIs/ScoreMaximizingAI.o libsquadro/AIs/ScoreEvaluators.o $(LIBSQUADRO_CORE_OBJS) $(LDFLAGS) -o Subprograms/Dedecker_Berry_IA -lm -lstdc++ -lstdc++fs
	cp -f Subprograms/Dedecker_Berry_IA Tournoi/IAs/

naive_AI: .depend Subprograms/naive_AI.o  libsquadro/AIs/ScoreMaximizingAI.o libsquadro/AIs/ScoreEvaluators.o $(LIBSQUADRO_CORE_OBJS)
	$(CC) $(CFLAGS) Subprograms/naive_AI.o  libsquadro/AIs/ScoreMaximizingAI.o libsquadro/AIs/ScoreEvaluators.o $(LIBSQUADRO_CORE_OBJS) $(LDFLAGS) -o Subprograms/naive_AI -lm -lstdc++ -lstdc++fs
	cp -f Subprograms/naive_AI Tournoi/IAs/

train: .depend Subprograms/MLPTrain.o $(LIBSQUADRO_EXTENDED_OBJS)
	$(CC) $(CFLAGS) Subprograms/MLPTrain.o $(LIBSQUADRO_EXTENDED_OBJS) $(LDFLAGS) -o Subprograms/MLPTrain $(LDLIBS) `pkg-config gtkmm-3.0 --libs`

depend: .depend

.depend: cmd = g++ -MM -MF depend $(var); cat depend >> .depend;
.depend:
	@echo "Generating dependencies..."
	@$(foreach var, $(LIBSQUADRO_EXTENDED_CPPS), $(cmd))
	@rm -f depend

-include .depend

# These are the pattern matching rules. In addition to the automatic
# variables used here, the variable $* that matches whatever % stands for
# can be useful in special cases.
%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@ `pkg-config gtkmm-3.0 --cflags`

%: %.cpp
	$(CC) $(CFLAGS) -o $@ $< `pkg-config gtkmm-3.0 --cflags`

clean:
	rm -f .depend $(LIBSQUADRO_EXTENDED_OBJS) Subprograms/*.o

.PHONY: clean depend
