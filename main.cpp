#include <iostream>
#include <string>

#include "macros.hpp"
#include "libsquadro/GameMenus/AbstractGameMenu.hpp"

/*Fonction main
 *Crée et lance le menu
 */
int main(int argc, char const *argv[]){
  AbstractGameMenu* p_gameMenu = AbstractGameMenu::getGameMenu(argc,argv);
  p_gameMenu->start();
  delete p_gameMenu;
  return 0;
}
