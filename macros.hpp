/*Infrastructure de debug   A
 *Si le code est compilé avec la macro "DO_DEBUG", DEBUG(x) sera remplacé par x
 *Sinon, DEBUG(x) sera ignoré
 */
#ifdef DO_DEBUG
#define DEBUG(x) x
#else
#define DEBUG(x)
#endif

/*Infrastruce de ASSERT   J
 *Structure de test
 */
#define ASSERT(C) if ( !(C) ) { std::cerr << "Test failed: "#C << std::endl; }
