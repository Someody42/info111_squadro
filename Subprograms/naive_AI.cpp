#include "../libsquadro/GameCore.hpp"
#include "../libsquadro/AIs/ScoreEvaluators.hpp"
#include "../libsquadro/AIs/ScoreMaximizingAI.hpp"

void IA_squadro(){
  GameCore* p_core = new GameCore(GameDifficulty::Easy, "", "resultat.txt");
  AdvancementEvaluator* p_evaluator = new AdvancementEvaluator();
  p_core->buildBoard("etat_jeu.txt");
  AbstractAI* p_AI = new TreeExploringBestScoreMaximizingAI(p_evaluator,p_core,p_core->getNextPlayer(),1);
  p_AI->play();
  while(p_core->fetchAction()){}
  delete p_core;
  delete p_AI;
  delete p_evaluator;
}

int main(){
  IA_squadro();
  return 0;
}
