#include "ExternalAI.hpp"

std::string ExternalAI::AI_path("Tournoi/IAs/");

ExternalAI::ExternalAI(std::string filename, GameCore* p_core, int playerID, bool notValidWarning) : AbstractAI(p_core,playerID), _filename(filename), _notValidWarning(notValidWarning) {

}

void ExternalAI::play() {
  std::experimental::filesystem::copy_file(_p_core->getSaveFile(),"etat_jeu.txt",
                                           std::experimental::filesystem::copy_options::overwrite_existing);
  std::ofstream resultFileW("resultat.txt");
  resultFileW.close();
  int error = std::system(("timelimit -T 1 ./" + AI_path + _filename).c_str());
  if(error){
    throw("Execution error");
  }
  std::ifstream resultFile("resultat.txt");
  int pieceMoved;
  resultFile >> pieceMoved;
  if(_playerID==1){
    pieceMoved = (int)_p_core->getDifficulty()+1-pieceMoved;
  }
  if(_notValidWarning && !_p_core->isValid(_playerID,pieceMoved)){
    std::cout<<"ATTENTION : Vous essayez de faire un mouvement invalide !!!"<<std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(4));
  }
  _p_core->pushAction(_playerID,pieceMoved);
}
