#include "../libsquadro/GameCore.hpp"
#include "../libsquadro/AIs/ScoreMaximizingAI.hpp"
#include "../libsquadro/AIs/ScoreEvaluators.hpp"
#include "../libsquadro/AIs/RandomAI.hpp"

int main(int argv, char** argc){
  int n = 10000;
  GameDifficulty difficulty = GameDifficulty::Easy;
  if(argv >= 2){
    n = std::stoi(std::string(argc[1]));
    difficulty = (GameDifficulty)std::stoi(std::string(argc[2]));
  }
  GameCore* p_core;
  RandomAI* p_ai0;
  RandomAI* p_ai1;
  MLPEvaluator* p_evaluator = new MLPEvaluator(difficulty);
  p_evaluator->loadPreTrained();
  for (size_t i = 0; i < (size_t)n; i++) {
    std::cout<<i<<std::endl;
    p_core = new GameCore(difficulty, "", "");
    p_ai0 = new RandomAI(p_core,0);
    p_ai1 = new RandomAI(p_core,1);
    p_core->buildBoard("");
    p_core->play();
    while(p_core->fetchAction()){}
    p_evaluator->feedAll(p_core->getActionLog(),p_core->getWinner());
    delete p_core;
    delete p_ai0;
    delete p_ai1;
    if(i%100==0){
      p_evaluator->save();
    }
  }
  p_evaluator->save();
  delete p_evaluator;
}
