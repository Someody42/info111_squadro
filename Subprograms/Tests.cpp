#include "../macros.hpp"
#include "../libsquadro/GameCore.hpp"
#include "../libsquadro/utilities.hpp"

#define ASSERT(C) if ( !(C) ) { std::cerr << "Test failed: "#C << std::endl; }

std::vector<std::vector<CellState>> convertBoard(std::vector<std::vector<char>> v){
  std::vector<std::vector<CellState>> ret(v.size(),std::vector<CellState>(v[0].size(),CellState::None));
  for (size_t i = 0; i < v.size(); i++) {
    for (size_t j = 0; j < v[0].size(); j++) {
      ret[i][j]=(CellState)v[i][j];
    }
  }
  return ret;
}

void testLinalg(){
  std::vector<int> i({1,0}), j({0,1}), u({2,2}), v({69,420});

  u*=2;
  ASSERT(u==std::vector<int>({4,4}))

  u+=j;
  ASSERT(u==std::vector<int>({4,5}))
  try{
    u+=std::vector<int>({0});
    ASSERT(false)
  }catch(...){

  }

  v*=i;
  ASSERT(v==std::vector<int>({69,0}))
  try{
    v*=std::vector<int>({1});
    ASSERT(false)
  }catch(...){

  }

  ASSERT(u+std::vector<int>(2,0)==u)
  ASSERT(u+v==std::vector<int>({73,5}))
  try{
    u+std::vector<int>({0});
    ASSERT(false)
  }catch(...){

  }

  ASSERT(u*u==std::vector<int>({16,25}))
  ASSERT(i*u==4*i)
  try{
    v*std::vector<int>({1});
    ASSERT(false)
  }catch(...){

  }

  ASSERT(4*i==std::vector<int>({4,0}))
  ASSERT(4*i+5*j==u)

  ASSERT(-u==std::vector<int>({-4,-5}))
  v=std::vector<int>({69,0});
  v-=u;
  ASSERT(v==std::vector<int>({65,-5}))

  ASSERT(v-u==std::vector<int>({61,-10}))
  ASSERT(v-(-u)==v+u)
  try{
    u-std::vector<int>({0});
    ASSERT(false)
  }catch(...){

  }

  std::vector<std::vector<int>> Id({{1,0},
                                    {0,1}}),
                                Shear({{1,1},
                                       {0,1}}),
                                Flip({{0,1},
                                      {1,0}}),
                                Rotate({{0,-1},
                                        {1,0}}),
                                NonSquare({{3,4,2},
                                           {0,6,5}});

  ASSERT(transpose(Id)==Id);
  ASSERT(transpose(Shear)==std::vector<std::vector<int>>({{1,0},
                                                          {1,1}}))

  ASSERT(matProd(Id,Shear)==Shear)
  ASSERT(matProd(Shear,Id)==Shear)
  ASSERT(matProd(Shear,Rotate)==std::vector<std::vector<int>>({{1,-1},
                                                               {1,0}}))
  ASSERT(matProd(Rotate,Flip)==std::vector<std::vector<int>>({{-1,0},
                                                              {0,1}}))
  ASSERT(matProd(Flip,Rotate)==std::vector<std::vector<int>>({{1,0},
                                                              {0,-1}}))

  ASSERT(matProd(Id,NonSquare)==NonSquare)

  try{
    matProd(Id,std::vector<std::vector<int>>({{0}}));
    ASSERT(false)
  }catch(...){

  }

  ASSERT(matVProd(Id,u)==u)
  ASSERT(matVProd(Rotate,u)==std::vector<int>({-5,4}))

  try{
    matVProd(Rotate,std::vector<int>({0}));
    ASSERT(false)
  }catch(...){

  }

}

void testGameCore(){
  GameCore* p_core;
  //NEW
  std::vector<std::vector<char>> test1_a(
  {{' ',' ',' ',' ',' ',' ',' '},
   {'>',' ',' ',' ',' ',' ',' '},
   {'>',' ',' ',' ',' ',' ',' '},
   {'>',' ',' ',' ',' ',' ',' '},
   {' ',' ','>',' ',' ',' ',' '},
   {' ',' ','<',' ',' ',' ',' '},
   {' ','^','^','^','^','^',' '}});
   //J1,1,J2,2/J1,3
  auto t1_a = convertBoard(test1_a);

  std::vector<std::vector<char>> test1_b(
  {{' ',' ',' ',' ',' ',' ',' '},
   {' ','>',' ',' ',' ',' ',' '},
   {'>',' ',' ',' ',' ',' ',' '},
   {'>',' ','^',' ',' ',' ',' '},
   {'>',' ',' ',' ',' ',' ',' '},
   {' ',' ',' ',' ',' ',' ','<'},
   {' ','^',' ','^','^','^',' '}});
   //J1,3
  auto t1_b = convertBoard(test1_b);

  std::vector<std::vector<char>> test1_c(
  {{' ',' ',' ',' ',' ',' ',' '},
   {' ','>',' ',' ',' ',' ',' '},
   {'>',' ',' ',' ',' ',' ',' '},
   {' ',' ',' ','>',' ',' ',' '},
   {'>',' ',' ',' ',' ',' ',' '},
   {' ',' ',' ',' ',' ',' ','<'},
   {' ','^','^','^','^','^',' '}});
  auto t1_c = convertBoard(test1_c);


  p_core = new GameCore(t1_a, GameDifficulty::Easy);
  p_core->pushAction(0,1);
  p_core->pushAction(1,2);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t1_b)

  p_core->pushAction(0,3);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t1_c)
  delete p_core;


//NEW
  std::vector<std::vector<char>> test2_a(
    {{' ',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>','^','v',' ',' ',' ',' '},
     {' ',' ',' ','^','^','^',' '}});
     //J1,5/J2,3
  auto t2_a = convertBoard(test2_a);

  std::vector<std::vector<char>> test2_b(
    {{' ',' ','v',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {' ',' ',' ','>',' ',' ',' '},
     {' ','^',' ','^','^','^',' '}});
     //J2,3
  auto t2_b = convertBoard(test2_b);

  std::vector<std::vector<char>> test2_c(
    {{' ',' ','v',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ','^',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {' ','^',' ',' ','^','^',' '}});
  auto t2_c = convertBoard(test2_c);


  p_core = new GameCore(t2_a, GameDifficulty::Easy);
  p_core->pushAction(0,5);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t2_b)

  p_core->pushAction(1,3);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t2_c)
  delete p_core;


//NEW
  std::vector<std::vector<char>> test3_a(
    {{' ',' ',' ',' ',' ',' ',' '},
     {'>','^',' ',' ',' ',' ',' '},
     {' ',' ',' ',' ',' ','>',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {' ',' ','^','^','^','^',' '}});
     //J1,2/J2,1
  auto t3_a = convertBoard(test3_a);

  std::vector<std::vector<char>> test3_b(
    {{' ',' ',' ',' ',' ',' ',' '},
     {'>','^',' ',' ',' ',' ',' '},
     {' ',' ',' ',' ',' ',' ','<'},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {' ',' ','^','^','^','^',' '}});
      //J2,1
  auto t3_b = convertBoard(test3_b);

  std::vector<std::vector<char>> test3_c(
    {{' ','v',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {' ',' ',' ',' ',' ',' ','<'},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {' ',' ','^','^','^','^',' '}});
  auto t3_c = convertBoard(test3_c);


  p_core = new GameCore(t3_a, GameDifficulty::Easy);
  p_core->pushAction(0,2);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t3_b)

  p_core->pushAction(1,1);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t3_c)
  delete p_core;


//NEW
  std::vector<std::vector<char>> test4_a(
    {{' ',' ',' ',' ',' ',' ',' '},
     {' ','<',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ','v',' '},
     {' ','^','^','^','^',' ',' '}});
     //J1,1/J1,1/J2,5/J2,5/J1,1/J1,2/J2,5
  auto t4_a = convertBoard(test4_a);

  std::vector<std::vector<char>> test4_b(
    {{' ',' ',' ',' ',' ',' ',' '},
     {'<',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ','v',' '},
     {' ','^','^','^','^',' ',' '}});
     //J1,1/J2,5/J2,5/J1,1/J1,2/J2,5
  auto t4_b = convertBoard(test4_b);

     //J2,5/J2,5/J1,1/J1,2/J2,5

  std::vector<std::vector<char>> test4_c(
    {{' ',' ',' ',' ',' ',' ',' '},
     {'<',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {' ','^','^','^','^','v',' '}});
     //J2,5/J1,1/J1,2/J2,5
  auto t4_c = convertBoard(test4_c);

     //J1,1/J1,2/J2,5

     //J1,2/J2,5

     //J2,5

  std::vector<std::vector<char>> test4_d(
    {{' ',' ',' ',' ',' ',' ',' '},
     {'<',' ',' ',' ',' ',' ',' '},
     {' ',' ',' ','>',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' '},
     {' ','^','^','^','^','v',' '}});
  auto t4_d = convertBoard(test4_d);


  p_core = new GameCore(t4_a, GameDifficulty::Easy);
  p_core->pushAction(0,1);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t4_b)
  ASSERT(p_core->getScore(0)==1)

  p_core->pushAction(0,1);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t4_b)
  ASSERT(p_core->getScore(0)==1)

  p_core->pushAction(1,5);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t4_c)
  ASSERT(p_core->getScore(1)==1)

  p_core->pushAction(1,5);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t4_c)
  ASSERT(p_core->getScore(1)==1)

  p_core->pushAction(0,1);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t4_c)
  ASSERT(p_core->getScore(0)==1)

  p_core->pushAction(0,2);
  p_core->pushAction(1,5);
  while(p_core->fetchAction());
  ASSERT(p_core->getBoard()==t4_d)
  ASSERT(p_core->getScore(1)==1)

  delete p_core;

  std::vector<std::vector<char>> test5_a(
    {{' ','v',' ',' ',' ',' ',' ',' ',' '},
     {'>',' ',' ',' ',' ',' ',' ',' ',' '},
     {'<',' ',' ',' ',' ',' ',' ',' ',' '},
     {'<',' ',' ',' ',' ',' ',' ',' ',' '},
     {'<',' ',' ',' ',' ',' ',' ',' ',' '},
     {'<',' ',' ',' ',' ',' ',' ',' ',' '},
     {' ',' ','<',' ',' ',' ',' ',' ',' '},
     {'<',' ',' ',' ',' ',' ',' ',' ',' '},
     {' ',' ','^','^','^','^','^','^',' '}});

  auto t5_a = convertBoard(test5_a);

  //On écrase tout
  std::ofstream file("tests_out.save");
  file.close();
  file.open("tests.log");
  file.close();

  p_core = new GameCore(GameDifficulty::Easy,"tests_out.save","tests.log");
  p_core->buildBoard("tests.save");
  ASSERT(p_core->getDifficulty()==GameDifficulty::Normal)
  ASSERT(p_core->getBoard()==t5_a)//La rotation a bien été faite
  ASSERT(p_core->getScore(1)==0)
  ASSERT(p_core->getScore(0)==5)//Les scores ont bien étés reconstruits
  p_core->pushAction(1,1);
  while(p_core->fetchAction()){}
  //Vérification de la sauvegarde

  std::ifstream saveFile("tests_out.save");
  std::string content((std::istreambuf_iterator<char>(saveFile)),(std::istreambuf_iterator<char>()));
  std::string theoricalOutput = "  1 2 3 2 3 2 1 \nx . . . . . . . x \n> v . . . . . . . \n< . . . . . . . . \n< . . . . . . . . \n< . . . . . . . . \n< . . . . . . . . \n. . < . . . . . . \n< . . . . . . . . \nx . ^ ^ ^ ^ ^ ^ x \n";
  ASSERT(content==theoricalOutput)
  saveFile.close();

  p_core->pushAction(0,6);
  while(p_core->fetchAction()){}
  //Vérification des logs
  std::ifstream logFile("tests.log");
  content="";
  content.assign((std::istreambuf_iterator<char>(logFile)),(std::istreambuf_iterator<char>()));
  std::string theoricalLog = "1,6,";
  ASSERT(content == theoricalLog)
  logFile.close();

  ASSERT(p_core->gameEnded())//Détection de la victoire fonctionnelle
  ASSERT(p_core->getWinner()==0)//Détection du vainqueur fonctionnelle



  delete p_core;
}

int main(int argc, char const *argv[]) {
  testLinalg();
  testGameCore();
  return 0;
}
