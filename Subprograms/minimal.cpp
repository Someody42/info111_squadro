#include <iostream>
#include <string>

#include "../macros.hpp"
#include "../libsquadro/GameCore.hpp"
#include "../libsquadro/Interfaces/AbstractInterface.hpp"
#include "../libsquadro/AIs/ScoreEvaluators.hpp"
#include "../libsquadro/AIs/ScoreMaximizingAI.hpp"

int main(int argc, char const *argv[]){
  if(argc <= 1) return 0;
  int ai_number = std::stoi(argv[1]);
  if(ai_number != 1 && ai_number != 0) return 0;
  GameCore* p_core = new GameCore;

  try{
    p_core->buildBoard("squadro.save");
  }catch(...){
    std::cout<<"Erreur lors du chargement du fichier. Lancement d'une nouvelle partie..."<<std::endl;
    p_core->buildBoard("");
  }

  AbstractInterface* p_interface = new CLIInterface(p_core);

  AdvancementEvaluator* p_evaluator = new AdvancementEvaluator();
  AbstractAI* p_ai = new TreeExploringBestScoreMaximizingAI(p_evaluator, p_core,ai_number,8);

  p_core->play();

  delete p_ai;
  delete p_evaluator;
  delete p_interface;

  delete p_core;
  return 0;
}
