#include "../libsquadro/GameCore.hpp"
#include "ExternalAI.hpp"
#include "../libsquadro/Interfaces/CLIInterface.hpp"

#include <map>
#include <thread>
#include <chrono>
#include <experimental/filesystem>

int play(std::string y, std::string r){
  GameCore* p_core = new GameCore(GameDifficulty::Easy, "squadro.save", std::string("Tournoi/Resultats/") + y + "-" + r + ".save",150);
  p_core->buildBoard("");
  p_core->save();
  AbstractAI* p_AI0 = new ExternalAI(y,p_core,0,true);
  AbstractAI* p_AI1 = new ExternalAI(r,p_core,1,true);
  AbstractInterface* p_interface = new CLIInterface(p_core,true);
  p_core->play();
  int ret=p_core->getWinner();
  delete p_interface;
  delete p_core;
  delete p_AI1;
  delete p_AI0;
  return ret;
}

int main(){
  int winner;
  std::map<std::string,int> programs;
  for(auto& p : std::experimental::filesystem::directory_iterator(ExternalAI::AI_path)){
    if(is_regular_file(p)){
      programs[p.path().filename()] = 0;
    }
  }

  for(auto& keyval : programs){
    for(auto& keyval2 : programs){
      winner = play(keyval.first,keyval2.first);
      std::cout<<keyval.first<<" VS "<<keyval2.first<<" : "<<std::endl;
      if(winner==0){
        keyval.second+=2;
        std::cout<<"Victoire du joueur jaune"<<std::endl;
      }else if(winner==1){
        keyval2.second+=2;
        std::cout<<"Victoire du joueur rouge"<<std::endl;
      }else{
        keyval.second++;
        keyval2.second++;
        std::cout<<"Ex aequo"<<std::endl;
      }
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
  }

  for(auto const& keyval : programs){
    std::cout<<keyval.first << " : "<<keyval.second<<std::endl;
  }
  return 0;
}
