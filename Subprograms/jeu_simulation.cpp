#include "../libsquadro/GameCore.hpp"
#include "ExternalAI.hpp"
#include "../libsquadro/Interfaces/CLIInterface.hpp"

#include <map>
#include <thread>
#include <chrono>
#include <experimental/filesystem>

int play(std::string y, std::string r){
  GameCore* p_core = new GameCore(GameDifficulty::Easy, "squadro.save", std::string("Tournoi/Resultats/") + y + "-" + r + ".save",150);
  p_core->buildBoard("");
  p_core->save();
  AbstractAI* p_AI0 = new ExternalAI(y,p_core,0,true);
  AbstractAI* p_AI1 = new ExternalAI(r,p_core,1,true);
  AbstractInterface* p_interface = new CLIInterface(p_core);
  p_core->play();
  int ret=p_core->getWinner();
  delete p_interface;
  delete p_core;
  delete p_AI1;
  delete p_AI0;
  return ret;
}

int main(int argc, char const *argv[]){
  std::string errorMessage("Usage : ./jeu_simulation [Nom de votre programme]\nAttention,votre programme doit être dans le dossier Tournoi/IAs/");
  if(argc==1){
    std::cout<<errorMessage<<std::endl;
    return 0;
  }

  int winner;

  std::string adv = argv[1];
  winner = play("naive_AI",adv);
  if(winner==0){
    std::cout<<"Victoire du joueur naïf"<<std::endl;
  }else if(winner==1){
    std::cout<<"Victoire de votre IA"<<std::endl;
  }else{
    std::cout<<"Ex aequo"<<std::endl;
  }
  std::this_thread::sleep_for(std::chrono::seconds(1));

  winner = play(adv,"naive_AI");
  if(winner==0){
    std::cout<<"Victoire de votre IA"<<std::endl;
  }else if(winner==1){
    std::cout<<"Victoire du joueur naïf"<<std::endl;
  }else{
    std::cout<<"Ex aequo"<<std::endl;
  }
  std::this_thread::sleep_for(std::chrono::seconds(1));
}
