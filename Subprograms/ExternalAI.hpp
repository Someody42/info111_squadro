#include "../libsquadro/AIs/AbstractAI.hpp"

#include <experimental/filesystem>
#include <cstdlib>
#include <fstream>
#include <thread>
#include <chrono>

class ExternalAI : public AbstractAI{
private:
  std::string _filename;
  bool _notValidWarning;

public:
  ExternalAI(std::string filename,GameCore* p_core, int playerID=1, bool notValidWarning = false);
  virtual void play();
  virtual ~ExternalAI() = default;

  static std::string AI_path;
};
