#ifndef GTK_GAME_MENU_HPP
#define GTK_GAME_MENU_HPP

#include <gtkmm.h>
#include <vector>

#include "AbstractGameMenu.hpp"


/*Classe GTKGameMenu    AJ
 *Crée un menu en fenêtre pour choisir les différents paramètres du jeu
 */
class GTKGameMenu : public AbstractGameMenu{
private:
  /*Classe interne GTKMenuWindow
   *Hérite de Gtk::Window
   *Organise et gère les widgets du menu
   */
  class GTKMenuWindow : public Gtk::Window{
  private:
    GTKGameMenu* _p_menu;
    Gtk::RadioButton::Group _interfaceRBGroup, _difficultyRBGroup;
    Gtk::RadioButton _curseInterfaceRB, _SFMLInterfaceRB, _TTYInterfaceRB, _easyDifficultyRB, _normalDifficultyRB;
    Gtk::Entry _savePathEntry, _loadPathEntry;
    Gtk::Label _labelLoadPath, _labelSavePath;
    Gtk::Button _start, _quit, _new;
    Gtk::Box _globalBox;
    Gtk::Box _interfaceBox, _saveSystemBox, _loadPathBox, _savePathBox, _difficultyBox, _AIsBox, _AI0Box, _AI1Box;
    Gtk::Frame _frameInterface, _frameSaveSystem, _frameDifficulty, _frameAI0, _frameAI1;
    Gtk::ComboBoxText _comboBoxAI0, _comboBoxAI1;
  public:
    /*Constructeur de GTKMenuWindow
     *Crée et agence les différents widgets
     *@param GTKGameMenu* p_menu : un pointeur vers le menu GTK correspondant
     *Complexité : dépends des complexité de la bibliothèque GTK. EN dehors de ça,
     *complexité constante
     */
    GTKMenuWindow(GTKGameMenu* p_menu);

    /*Méthode refreshValues
     *Adapte le contenu des widgtes selon les paramètres rentrés en ligne de commande
     *Complexité (hors fonctions GTK) : O(log(_params.size()))
     */
    void refreshValues();

    /*Méthode on_delete_event
     *Arrête GTK et ferme le menu lorsqu'on ferme la fenêtre
     *@return true
     */
    bool on_delete_event(GdkEventAny* event);

    /*Fonctions d'accès aux paramètres
     *Permettent de récupérer les valeurs entrées dans le menu et d'en déduire les
     *paramètres correspondant
     *Complexités (hors fonctions GTK) constantes
     */
    std::string getInterface();
    std::string getDifficulty();
    std::string getSavePath();
    std::string getLoadPath();
    std::string getAI(int n);

    /*Destructeur de GTKMenuWindow
     */
    virtual ~GTKMenuWindow() = default;
  };

  Glib::RefPtr<Gtk::Application> _p_app;
  GTKMenuWindow* _p_window;
  bool _newGame;

public:
  /*Constructeur de GTKGameMenu
   *Complexité : complexité de création d'une application et d'une fenêtre GTK
   */
  GTKGameMenu();

  /*Méthode quit
   *Arrète le menu et ferme la fenêtre
   *Complexité : complexité de la méthode hide d'une fenêtre GTK
   */
  void quit();

  /*Spécialisation de la méthode start de AbstractGameMenu
   *Adapte les paramètres et lance l'application GTK
   *Complexité : complexité de l'application GTK + complexité du jeu
   */
  virtual int start();

  /*Méthode startGame
   *Mets à jour _params
   *Appelle la méthode start de AbstractGameMenu
   *Mets le menu en pause (désactive les button)
   *Tant que le jeu est lancé
   *Complexité : O(log(_params.size())) + complexité de AbstractInterface::start
   */
  virtual void startGame();

  /*Destructeur de GTKGameMenu
   */
  virtual ~GTKGameMenu();

  //Noms human-readable des IA
  static std::map<std::string,std::string> AINames;
};

#endif
