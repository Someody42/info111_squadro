#include "CLIGameMenu.hpp"

CLIGameMenu::CLIGameMenu() : AbstractGameMenu() {

}

int CLIGameMenu::start() {
  if(map_contains(_params,"quit")){
    return 0;
  }
  std::string buf;
  std::cout<<std::endl;
  for(auto const& keyval : AbstractGameMenu::labels){
    std::cout<<keyval.second<<" ["<<_params[keyval.first]<<"] : ";
    std::getline(std::cin,buf);
    _params[keyval.first] = tolower((buf=="") ? _params[keyval.first] : buf);
  }
  std::cout<<buf<<std::endl;
  initGame();
  return AbstractGameMenu::start();
}
