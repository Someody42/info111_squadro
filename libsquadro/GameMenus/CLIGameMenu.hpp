#ifndef CLI_GAME_MENU
#define CLI_GAME_MENU

#include<iostream>

#include "AbstractGameMenu.hpp"

/*Classe CLIGameMenu    A
 *Crée un menu en ligne de commande décrivant
 *les méthodes de base héritées de abstract interface
 */
class CLIGameMenu : public AbstractGameMenu{
public:
  /*Constructeur de CLIGameMenu
   *Complexité : O(1)
   */
  CLIGameMenu();

  /*Spécialisation de la méthode start de AbstractGameMenu
   *Gère l'affichage du menu et relègue le démarrage de la partie à leur ancètre
   *i.e la méthode start de AbstractGameMenu
   *Complexité : O(log(_params.size())) + complexité de la lecture/écriture en console
   * + complexité de AbstractGameMenu::start
   */
  virtual int start();

  /*Destructeur de CLIGameMenu
  */
  virtual ~CLIGameMenu() = default;
};

#endif
