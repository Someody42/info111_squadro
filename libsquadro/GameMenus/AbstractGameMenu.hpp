#ifndef ABSTRACT_GAME_MENU_HPP
#define ABSTRACT_GAME_MENU_HPP

#include <vector>
#include <map>
#include <algorithm>

#include "../GameCore.hpp"
#include "../Interfaces/AbstractInterface.hpp"
#include "../Interfaces/CLIInterface.hpp"
#include "../Interfaces/SFMLInterface.hpp"

#include "../AIs/AbstractAI.hpp"
#include "../AIs/RandomAI.hpp"
#include "../AIs/LinearAI.hpp"
#include "../AIs/ScoreMaximizingAI.hpp"
#include "../AIs/ScoreEvaluators.hpp"
#include "../utilities.hpp"

class CurseInterface;

/*Classe AbstractGameMenu   A
 *Classe abstraite décrivant les méthodes de base que doivent satisfairent les différents menus
 */
class AbstractGameMenu{
protected:
  bool _quit;
  GameCore* _p_core;
  AbstractInterface* _p_interface;
  std::vector<AbstractAI*> _vp_AIs;
  std::string _loadPath;
  std::string _savePath;
  std::vector<AbstractScoreEvaluator*> _vp_evaluators;
  std::map<std::string,std::string> _params;
public:
  /*Constructeur de AbstractGameMenu
   *Initialise tous les pointeurs à nullptr
   *Complexité : O(1)
   */
  AbstractGameMenu();

  /*Méthode initGame()
   *Initialise la partie en fonction des paramètres
   *Complexité : O(log(_params.size()))
   */
  void initGame();

  /*Méthode start()
   *Lance le menu (mais celui-ci est vide)
   *S'occupe de démarrer le jeu
   *Complexité : complexité des différents destructeurs et de la libération mémoire
   * + complexité du jeu en lui-même
   */
  virtual int start();

  /*Destructeur de AbstractGameMenu
   */
  virtual ~AbstractGameMenu() = default;

  /*Méthode statique getGameMenu
   *Génère les paramètres et le menu du jeu en fonction des arguments du programme
   *Complexité : O(argc) + complexité de construction de menu (+ complexité de initGame si pas de menu)
   */
  static AbstractGameMenu* getGameMenu(int argc, char const *argv[]);

  //Noms human-readable des paramètres
  static std::map<std::string,std::string> labels;

  //Paramètres raccourcis
  static std::map<std::string,std::vector<std::string>> shortcuts;
};

#include "../GameMenus/CLIGameMenu.hpp"
#include "../GameMenus/GTKGameMenu.hpp"
#include "../Interfaces/CurseInterface.hpp"

#endif
