#include "AbstractGameMenu.hpp"

std::map<std::string,std::string> AbstractGameMenu::labels({{"interface","Interface du jeu"},
                                                            {"difficulty","Difficulté du jeu"},
                                                            {"ai2","Intelligence artificielle du joueur 2 (\"None\" pour humain)"},
                                                            {"ai1","Intelligence artificielle du joueur 1 (\"None\" pour humain)"},
                                                            {"loadPath","Fichier de sauvegarde à charger (\"None\" pour reprendre à zéro)"},
                                                            {"savePath","Fichier pour sauvegarder la partie  (\"None\" pour ne rien sauvegarder)"}});

std::map<std::string,std::vector<std::string>> AbstractGameMenu::shortcuts({{"new",{"loadPath=None"}},
                                                                            {"ju",{"loadPath=None","interface=c","ai1=l"}},
                                                                            {"an",{"menu=gtk","interface=s","ai1=None","ai2=aa"}},
                                                                            {"noMenu",{"menu=None"}},
                                                                            {"noGUI",{"menu=tty","interface=curse"}}});

AbstractGameMenu::AbstractGameMenu() : _vp_AIs(2,nullptr), _vp_evaluators(2,nullptr) {
  _p_interface = nullptr;
  _quit = false;
}

void AbstractGameMenu::initGame(){
  //Soit ils étaient déjà à nullptr, soit ils pointaient sur de la mémoire libérée
  _p_interface = nullptr;
  _vp_AIs = {nullptr,nullptr};
  _vp_evaluators = {nullptr,nullptr};

  GameDifficulty gd = GameDifficulty::Easy;
  //Gestion des paramètres obligatoires (normalement ils existent tous)
  if(map_contains(_params,"quit")){
    _quit=true;
  }

  if (map_contains(_params,"difficulty")) {
    if(_params["difficulty"] == "e" || _params["difficulty"] == "easy") {
      _params["difficulty"] = "easy";
      gd = GameDifficulty::Easy;
    }else if(_params["difficulty"] == "n" || _params["difficulty"] == "normal"){
      _params["difficulty"] = "normal";
      gd = GameDifficulty::Normal;
    }
  }

  if (map_contains(_params,"savePath")) {
    _savePath = (_params["savePath"]=="None") ? "" : _params["savePath"];
    _params["savePath"] = _savePath;
  }

  _p_core = new GameCore(gd,_savePath);

  if (map_contains(_params,"loadPath")) {
    _loadPath = (_params["loadPath"]=="None") ? "" : _params["loadPath"];
    _params["loadPath"]=_loadPath;
  }

  try{
    _p_core->buildBoard(_loadPath);
  }catch(...){
    std::cout<<"Erreur lors du chargement du fichier. Lancement d'une nouvelle partie..."<<std::endl;
    _p_core->buildBoard("");
  }

  if(map_contains(_params,"interface")){
    if(_params["interface"] == "s" || _params["interface"] == "sfml") {
      _params["interface"] = "sfml";
      _p_interface = new SFMLInterface(_p_core);
    }else if(_params["interface"] == "c" || _params["interface"] == "curse"){
      _params["interface"] = "curse";
      _p_interface = new CurseInterface(_p_core);
    }else if(_params["interface"] == "t" || _params["interface"] == "cli" || _params["interface"] == "tty"){
      _params["interface"] = "tty";
      _p_interface = new CLIInterface(_p_core);
    }
  }

  if (map_contains(_params,"ai1")) {
    if(_params["ai1"] == "r" || _params["ai1"] == "random") {
      _params["ai1"] = "random";
      _vp_AIs[0] = new RandomAI(_p_core,0);
    } else if(_params["ai1"] == "l" || _params["ai1"] == "linear"){
      _params["ai1"] = "linear";
      _vp_AIs[0] = new LinearAI(_p_core,0);
    } else if(_params["ai1"] == "na" || _params["ai1"] == "naive_advancement"){
      _params["ai1"] = "naive_advancement";
      _vp_evaluators[0]=new AdvancementEvaluator();
      _vp_AIs[0] = new ScoreMaximizingAI(_vp_evaluators[0], _p_core,0);
    } else if(_params["ai1"] == "ba" || _params["ai1"] == "better_advancement"){
      _params["ai1"] = "better_advancement";
      _vp_evaluators[0]=new AdvancementEvaluator();
      _vp_AIs[0] = new TreeExploringBestScoreMaximizingAI(_vp_evaluators[0], _p_core,0,2);
    } else if(_params["ai1"] == "aa" || _params["ai1"] == "advanced_advancement"){
      int depth = (_p_core->getDifficulty()==GameDifficulty::Easy) ? 8 : 5;
      _params["ai1"] = "advanced_advancement";
      _vp_evaluators[0]=new AdvancementEvaluator();
      _vp_AIs[0] = new TreeExploringBestScoreMaximizingAI(_vp_evaluators[0], _p_core,0,depth);
    } else if(_params["ai1"] == "n" || _params["ai1"] == "neural_net"){
      _params["ai1"] = "neural_net";
      MLPEvaluator* p_evaluator = new MLPEvaluator(_p_core->getDifficulty());
      p_evaluator->loadPreTrained();
      _vp_evaluators[0]=p_evaluator;
      _vp_AIs[0] = new ScoreMaximizingAI(_vp_evaluators[0], _p_core,0);
    }
  }

  if (map_contains(_params,"ai2")) {
    if(_params["ai2"] == "r" || _params["ai2"] == "random") {
      _params["ai2"] = "random";
      _vp_AIs[1] = new RandomAI(_p_core,1);
    } else if(_params["ai2"] == "l" || _params["ai2"] == "linear"){
      _params["ai2"] = "linear";
      _vp_AIs[1] = new LinearAI(_p_core,1);
    } else if(_params["ai2"] == "na" || _params["ai2"] == "naive_advancement"){
      _params["ai2"] = "naive_advancement";
      _vp_evaluators[1] = new AdvancementEvaluator();
      _vp_AIs[1] = new ScoreMaximizingAI(_vp_evaluators[1], _p_core,1);
    } else if(_params["ai2"] == "ba" || _params["ai2"] == "better_advancement"){
      _params["ai2"] = "better_advancement";
      _vp_evaluators[1] = new AdvancementEvaluator();
      _vp_AIs[1] = new TreeExploringBestScoreMaximizingAI(_vp_evaluators[1], _p_core,1,2);
    } else if(_params["ai2"] == "aa" || _params["ai2"] == "advanced_advancement"){
      _params["ai2"] = "advanced_advancement";
      int depth = (_p_core->getDifficulty()==GameDifficulty::Easy) ? 8 : 5;
      _vp_evaluators[1] = new AdvancementEvaluator();
      _vp_AIs[1] = new TreeExploringBestScoreMaximizingAI(_vp_evaluators[1], _p_core,1,depth);
    } else if(_params["ai2"] == "n" || _params["ai2"] == "neural_net"){
      _params["ai2"] = "neural_net";
      MLPEvaluator* p_evaluator = new MLPEvaluator(_p_core->getDifficulty());
      p_evaluator->loadPreTrained();
      _vp_evaluators[1]=p_evaluator;
      _vp_AIs[1] = new ScoreMaximizingAI(_vp_evaluators[1], _p_core,1);
    }
  }
}

int AbstractGameMenu::start() {
  if(_quit){
    return 0;
  }
  int ret = _p_core->play();
  delete _p_core;
  if(_p_interface!=nullptr){
    delete _p_interface;
  }
  delete _vp_AIs[0];
  delete _vp_AIs[1];
  delete _vp_evaluators[0];
  delete _vp_evaluators[1];
  return ret;
}

AbstractGameMenu* AbstractGameMenu::getGameMenu(int argc, char const *argv[]) {
  std::string arg;
  std::string argname;
  std::string argval;
  std::queue<std::string> convertedArgv;
  std::map<std::string,std::string> defaultArgs({{"loadPath","squadro.save"},{"savePath","squadro.save"},{"menu","gtk"},{"ai1","None"},{"ai2","None"},{"difficulty","e"},{"interface","s"}});
  std::map<std::string,std::string> args;

  //On lit les arguments
  for (size_t i = 1; i < (unsigned int) argc; i++) {
    arg = std::string(argv[i]);
    if(map_contains(shortcuts,arg)){
      for(std::string narg : shortcuts[arg]){
        convertedArgv.push(narg);
      }
    }else{
      convertedArgv.push(arg);
    }
  }
  while(!convertedArgv.empty()){
    arg = convertedArgv.front();
    argname = "";
    argval = "";
    auto it_equal = std::find(arg.begin(),arg.end(),'=');
    if(it_equal!=arg.end()){
      std::copy(arg.begin(),it_equal,std::back_inserter(argname));//std::back_inserter(c) est un irérateur permettant de rajouter des éléments à c
      std::copy(it_equal+1,arg.end(),std::back_inserter(argval));
      args[argname] = tolower(argval);
    }else{
      std::copy(arg.begin(),it_equal,std::back_inserter(argname));
      args[argname]="";
    }
    convertedArgv.pop();
  }

  AbstractGameMenu* p_ret;
  bool noMenu = false;

  //On complète avec les arguments par défaut
  for(auto const& keyval : defaultArgs) {
    if(!map_contains(args,keyval.first)){
      args[keyval.first] = keyval.second;
    }
  }

  if(map_contains(args,"menu")){
    if(args["menu"]=="tty"){
      p_ret = new CLIGameMenu;
    }else if(args["menu"]=="gtk"){
      p_ret = new GTKGameMenu;
    }else{
      p_ret = new AbstractGameMenu;
      noMenu = true;
    }
  }else{
    p_ret = new AbstractGameMenu;
    noMenu=true;
  }

  p_ret->_params = args;
  if(noMenu){
    p_ret->initGame();
  }
  return p_ret;
}
