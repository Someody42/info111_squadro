#include "GTKGameMenu.hpp"

std::map<std::string,std::string> GTKGameMenu::AINames({{"Humain","None"},
                                                        {"Aléatoire","random"},
                                                        {"Linéaire","linear"},
                                                        {"Avancement naïf","naive_advancement"},
                                                        {"Avancement","better_advancement"},
                                                        {"Avancement avancé","advanced_advancement"},
                                                        {"MLP","neural_net"}});

GTKGameMenu::GTKGameMenu() : AbstractGameMenu(){
  _newGame = false;
  _quit = false;
  _p_app = Gtk::Application::create();
  _p_window = new GTKMenuWindow(this);
}


int GTKGameMenu::start(){
  //On assigne aux widgets les valeurs déjà déterminées
  _p_window->refreshValues();

  _p_app->run(*_p_window);
  return 0;
}

void GTKGameMenu::startGame(){

  _params["interface"]=_p_window->getInterface();
  _params["difficulty"]=_p_window->getDifficulty();
  _params["savePath"]=_p_window->getSavePath();
  _params["loadPath"]=_newGame ? "None" : _p_window->getLoadPath();
  _params["ai1"]=_p_window->getAI(0);
  _params["ai2"]=_p_window->getAI(1);
  _newGame=false;

  _p_app->hold();
  initGame();
  AbstractGameMenu::start();
  _p_app->release();
  return;
}

GTKGameMenu::~GTKGameMenu(){
  delete _p_window;
}

void GTKGameMenu::quit(){
  _quit=true;
  _p_window->hide();
}

GTKGameMenu::GTKMenuWindow::GTKMenuWindow(GTKGameMenu* p_menu) : Gtk::Window(),
                                                                 _curseInterfaceRB("CurseInterface"),
                                                                 _SFMLInterfaceRB("SFMLInterface"),
                                                                 _TTYInterfaceRB("CLIInterface"),
                                                                 _easyDifficultyRB("Facile (5x5)"),
                                                                 _normalDifficultyRB("Normal (7x7)"),
                                                                 _labelLoadPath("Fichier de sauvegarde à charger"),
                                                                 _labelSavePath("Fichier de sauvegarde à écrire"),
                                                                 _start("Lancer la partie",true),
                                                                 _quit("Quitter l'application",true),
                                                                 _new("Lancer une nouvelle partie",true),
                                                                 _globalBox(Gtk::ORIENTATION_VERTICAL),
                                                                 _interfaceBox(Gtk::ORIENTATION_HORIZONTAL),
                                                                 _saveSystemBox(Gtk::ORIENTATION_VERTICAL),
                                                                 _loadPathBox(Gtk::ORIENTATION_HORIZONTAL),
                                                                 _savePathBox(Gtk::ORIENTATION_HORIZONTAL),
                                                                 _difficultyBox(Gtk::ORIENTATION_HORIZONTAL),
                                                                 _AIsBox(Gtk::ORIENTATION_HORIZONTAL),
                                                                 _AI0Box(Gtk::ORIENTATION_VERTICAL),
                                                                 _AI1Box(Gtk::ORIENTATION_VERTICAL),
                                                                 _frameInterface("Choix de l'interface"),
                                                                 _frameSaveSystem("Préférences de sauvegarde"),
                                                                 _frameDifficulty("Choix de la difficulté"),
                                                                 _frameAI0("Joueur 1"),
                                                                 _frameAI1("Joueur 2")

{
  _p_menu = p_menu;
  //set_default_size(700,700);

  _globalBox.set_spacing(10);
  _globalBox.set_margin_left(10);
  _globalBox.set_margin_right(10);
  _globalBox.set_margin_top(10);
  _globalBox.set_margin_bottom(10);

  add(_globalBox);

  _globalBox.pack_start(_frameInterface);
  _frameInterface.add(_interfaceBox);
  //On ajoute les boutons de choix d'interface, reliés entre eux
  _TTYInterfaceRB.set_group(_interfaceRBGroup);
  _interfaceBox.pack_start(_TTYInterfaceRB);
  _SFMLInterfaceRB.set_group(_interfaceRBGroup);
  _interfaceBox.pack_start(_SFMLInterfaceRB);
  _curseInterfaceRB.set_group(_interfaceRBGroup);
  _interfaceBox.pack_start(_curseInterfaceRB);
  _SFMLInterfaceRB.set_active(true);

  _globalBox.pack_start(_frameSaveSystem);
  _frameSaveSystem.add(_saveSystemBox);
  _saveSystemBox.pack_start(_loadPathBox);
  _saveSystemBox.pack_start(_savePathBox);
  _loadPathBox.pack_start(_labelLoadPath);
  _loadPathBox.pack_start(_loadPathEntry);
  _loadPathEntry.set_text(_p_menu->_params["loadPath"]);
  _savePathBox.pack_start(_labelSavePath);
  _savePathBox.pack_start(_savePathEntry);
  _savePathEntry.set_text(_p_menu->_params["savePath"]);

  _globalBox.pack_start(_frameDifficulty);
  _frameDifficulty.add(_difficultyBox);
  _easyDifficultyRB.set_group(_difficultyRBGroup);
  _difficultyBox.pack_start(_easyDifficultyRB);
  _normalDifficultyRB.set_group(_difficultyRBGroup);
  _difficultyBox.pack_start(_normalDifficultyRB);

  _globalBox.pack_start(_AIsBox);
  _AIsBox.pack_start(_frameAI0);
  _frameAI0.add(_AI0Box);
  _AI0Box.pack_start(_comboBoxAI0);
  _AIsBox.pack_start(_frameAI1);
  _frameAI1.add(_AI1Box);
  _AI1Box.pack_start(_comboBoxAI1);
  for(auto keyval : AINames){
    _comboBoxAI0.append(keyval.second, keyval.first);
    _comboBoxAI1.append(keyval.second, keyval.first);
  }

  //On crée les boutons poussoirs utilitaires à l'application
  _globalBox.pack_start(_start);
  _globalBox.pack_start(_new);
  _globalBox.pack_start(_quit);
  _start.signal_clicked().connect(sigc::mem_fun(*_p_menu,&GTKGameMenu::startGame));
  _new.signal_clicked().connect([this](){_p_menu->_newGame=true;});
  _new.signal_clicked().connect(sigc::mem_fun(*_p_menu,&GTKGameMenu::startGame));
  _quit.signal_clicked().connect(sigc::mem_fun(*_p_menu,&GTKGameMenu::quit));

  show_all_children();
}

void GTKGameMenu::GTKMenuWindow::refreshValues(){
  _loadPathEntry.set_text(_p_menu->_params["loadPath"]);
  _savePathEntry.set_text(_p_menu->_params["savePath"]);

  if(_p_menu->_params["interface"]=="c" || _p_menu->_params["interface"]=="curse"){
    _curseInterfaceRB.set_active(true);
  }else if(_p_menu->_params["interface"]=="t" || _p_menu->_params["interface"]=="tty"){
    _TTYInterfaceRB.set_active(true);
  }else if(_p_menu->_params["interface"]=="s" || _p_menu->_params["interface"]=="sfml"){
    _SFMLInterfaceRB.set_active(true);
  }

  if(_p_menu->_params["difficulty"]=="e" || _p_menu->_params["difficulty"]=="easy"){
    _easyDifficultyRB.set_active(true);
  }else if(_p_menu->_params["difficulty"]=="n" || _p_menu->_params["difficulty"]=="normal"){
    _normalDifficultyRB.set_active(true);
  }

  _comboBoxAI0.set_active_id(_p_menu->_params["ai1"]);
  _comboBoxAI1.set_active_id(_p_menu->_params["ai2"]);
}

std::string GTKGameMenu::GTKMenuWindow::getInterface(){
  if(_curseInterfaceRB.get_active()){
    return "c";
  }else if(_TTYInterfaceRB.get_active()){
    return "t";
  }else if(_SFMLInterfaceRB.get_active()){
    return "s";
  }
  return "s";
}

std::string GTKGameMenu::GTKMenuWindow::getAI(int n){
  return (n==0) ? _comboBoxAI0.get_active_id() : _comboBoxAI1.get_active_id();
}

std::string GTKGameMenu::GTKMenuWindow::getDifficulty(){
  if(_easyDifficultyRB.get_active()){
    return "e";
  }else if(_normalDifficultyRB.get_active()){
    return "n";
  }
  return "e";
}

std::string GTKGameMenu::GTKMenuWindow::getSavePath(){
  return tolower(_savePathEntry.get_text());
}

std::string GTKGameMenu::GTKMenuWindow::getLoadPath(){
  return tolower(_loadPathEntry.get_text());
}

bool GTKGameMenu::GTKMenuWindow::on_delete_event(GdkEventAny* event){
  _p_menu->quit();
  return true;
}
