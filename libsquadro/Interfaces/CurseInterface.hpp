#ifndef CURSE_INTERFACE_HPP
#define CURSE_INTERFACE_HPP

#include <ncurses.h>
#include <string>
#include "../utilities.hpp"

#include "../../macros.hpp"
#include "AbstractInterface.hpp"

/*Classe CurseInterface                       J (pour la classe)
 *Hérite de AbstractInterface
 *Implémente une interface plus jolie que celle en CLI pour Squadro en console
 */
class CurseInterface : public AbstractInterface{
private:
  WINDOW * _messages;
  WINDOW * _mess;
  WINDOW * _score;
  WINDOW * _gameBoard;
  int Score0;
  int Score1;

public:
  /*Constructeur de CurseInterface
   *@param GameCore* p_core un pointeur vers le GameCore correspondant.
   *Initialise l'affichage du jeu dans plusieurs fenêtres
   *Complexité : O(difficulty^2) + complexité d'initialisation de curse
   */
  CurseInterface(GameCore* p_core);

  /*Méthode update
   *Hérite de la méthode update de AbstractInterface
   *Pour cette interface, ne fait rien
   */
  virtual void update();

  /*Méthode show
   *Hérite de la méthode show de AbstractInterface
   *@return int : 0 ssi tout s'est bien passé
   *Complexité (au pire) : O(nombreDeTours * difficulty^3) + complexité de l'affichage curse
   */
  virtual int show();

  /*Méthode displayBoard
   *Gère l'affichage de la position des pions;
   *Lit le plateau grâce à la méthode getBoard;
   *Complexité : O(difficulty^2)
   */
  virtual void displayBoard();

  /*Destructeur de CurseInterface
   *Signale la fin de l'utilisation de la librairie ncurses
   *Complexité : complexité de endwin
   */
  virtual ~CurseInterface();

  /*Spécialisation de la Méthode message() de AbstractInterface;
   *Affiche des messages précis en cas de victoire ou mouvement invalide
   *Complexité : O(1)
   */
  virtual void message(GameMessage);
};

#endif
