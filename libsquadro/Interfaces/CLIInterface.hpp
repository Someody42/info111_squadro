#ifndef CLI_INTERFACE_HPP
#define CLI_INTERFACE_HPP

#include <iostream>
#include <string>

#include "../../macros.hpp"
#include "../utilities.hpp"
#include "AbstractInterface.hpp"

/*Classe CLIInterface (Command Line Interface)    AJ
 *Hérite de AbstractInterface
 *Implémente une interface basique pour Squadro en console
 */
class CLIInterface : public AbstractInterface{
private:
  int _playingPlayer;
  bool _noDisp;
public:

  /*Constructeur de CLIInterface
   *@param GameCore* p_core un pointeur vers le GameCore correspondant.
   *Complexité : O(1)
   */
  CLIInterface(GameCore* p_core, bool noDisp=false);

  /*Méthode message
   *Reçoit des informations transmise par le GameCore
   *Afin d'afficher n'importe quel message à l'utilisateur au travers de l'interface
   *Par défaut, le message est affiché en console
   *@param std::wstring msg : la chaine de caractère à afficher
   *COmplexité : complexité de l'affichage en console
   */
  virtual void message(std::wstring msg);

  /*Méthode message
   *Reçoit les informations transmise par le GameCore sous forme de message caractéristique
   *Afin de les afficher à l'utilisateur au travers de l'interface;
   *@param GameMessage msg : le message à traiter
   *Appelle l'autre méthode message avec la chaîne correspondante
   *COmplexité : Complexité de l'affichage en console
   */
  virtual void message(GameMessage msg);

  /*Méthode update
   *Surcharge de la méthode update de AbstractInterface
   *Méthode appelée après qu'un coup ait été joué
   *Affichage du plateau
   *Complexité : O(difficulty^2)
   */
  virtual void update();

  /*Méthode show
   *Surcharge de la méthode show de AbstractInterface
   *Cette méthode gère l'affichage du jeu. Consiste en une boucle action/affichage
   *Demande au joueur d'entrer le numéro de la pièce à bouger
   *@return int : 0 ssi tout s'est bien passé, autre chose sinon
   *Complexité : O(difficulty^3) (si l'utilisateur rentre bien un entier)
   */
  virtual int show();

  /*Méthode displayBoard
   *Gère l'affichage du plateau
   *Lit le plateau grâce à la méthode getBoard
   *Complexité : O(difficulty^2)
   */
  virtual void displayBoard();

  /*Destructeur de la CLIInterface
   */
  virtual ~CLIInterface() = default;
};

#endif
