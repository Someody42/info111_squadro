#ifndef ABSTRACT_INTERFACE_HPP
#define ABSTRACT_INTERFACE_HPP

#include "../../macros.hpp"

#include <iostream>

enum class GameMessage : unsigned int;
class GameCore; //Problèmes de circularité : AbstractInterface veut connaître GameCore, qui veut connaître AbstractInterface

/*Classe AbstractInterface    A
 *Classe abstraite décrivant les méthodes de base que doivent satisfairent
 *les différentes interfaces
 */
class AbstractInterface{
protected:
  GameCore* _p_core;

public:

  /*Constructeur de AbstractInterface
   *@param GameCore* p_core un pointeur vers le GameCore correspondant.
   *Appelle linkInterface sur à l'objet pointé par p_core pour faire la synchronisation dans l'autre sens
   *Complexité : O(1)
   */
  AbstractInterface(GameCore* p_core);

  /*Méthode message
   *Reçoit des informations transmise par le GameCore
   *Afin d'afficher n'importe quel message à l'utilisateur au travers de l'interface
   *Par défaut, le message est affiché en console
   *@param std::wstring msg : la chaine de caractère à afficher
   *COmplexité : complexité de l'affichage en console
   */
  virtual void message(std::wstring msg);

  /*Méthode message
   *Reçoit les informations transmise par le GameCore sous forme de message caractéristique
   *Afin de les afficher à l'utilisateur au travers de l'interface;
   *@param GameMessage msg : le message à traiter
   *Appelle l'autre méthode message avec la chaîne correspondante
   *COmplexité : Complexité de l'affichage en console
   */
  virtual void message(GameMessage msg);

  /*Méthode update
   *Appelée à la fin de chaque coup pour faire les mises à jours nécessaires de l'interface
   *Serait particulièrement adaptée à un système de signaux et de slots
   */
  virtual void update() = 0;

  /*Méthode show
   *Gère l'affichage du jeu;
   *S'occupe des interactions avec le Joueur;
   *Communique avec le GameCore pour la gestion des actions et évènements;
   *@return 0 si tout s'est bien passé
   */
  virtual int show() = 0;

  /*Destructeur de AbstractInterface
   */
  virtual ~AbstractInterface() = default;
};

#include "../GameCore.hpp"

#endif
