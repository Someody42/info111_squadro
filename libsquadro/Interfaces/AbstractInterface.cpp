#include "AbstractInterface.hpp"

AbstractInterface::AbstractInterface(GameCore* p_core){
  _p_core = p_core;
  _p_core->linkInterface(this);
}

void AbstractInterface::message(std::wstring msg){
  std::wcout<<msg<<std::endl;
}

void AbstractInterface::message(GameMessage msg){
  message(GameCore::messageTexts[msg]);
}
