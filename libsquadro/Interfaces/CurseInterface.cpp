#include "CurseInterface.hpp"

CurseInterface::CurseInterface(GameCore* p_core) : AbstractInterface(p_core) {
  int difficulty;
  difficulty = (int)_p_core->getDifficulty();
  Score0 = (int)_p_core->getScore(0);
  Score1 = (int)_p_core->getScore(1);

  std::setlocale(LC_ALL, "");
  initscr();
  start_color();
  cbreak();
  noecho();


  init_pair(2, 6, COLOR_WHITE);
  init_pair(1, 2, COLOR_WHITE);
  init_pair(3, COLOR_BLACK, COLOR_WHITE);

  auto _speeds = _p_core->getSpeeds();
  int j = 0;

  _gameBoard = newwin(1+4*(difficulty+2), 1+2*4*(difficulty+2), 1, 2);
  wattrset(_gameBoard,COLOR_PAIR(3));

  for (int k = 0; k < 1+4*(difficulty+2); k++){
    for (int l = 0; l < 1+2*4*(difficulty+2); l++){
      waddch(_gameBoard, ' ');
    }
  }

  wborder(_gameBoard, 0, 0, 0, 0, 0, 0, 0, 0);
  for (int i = 4; i < 4*(difficulty+2); i += 4 ){
      mvwhline(_gameBoard, i, 1, ACS_HLINE, 2*4*(difficulty+2)-1);
    if(j<difficulty){
      mvwprintw(_gameBoard, i+3, 6, "%d", _speeds[0][j]);
      mvwprintw(_gameBoard, i+3, 2*4*(difficulty+2)-2, "%d", _speeds[1][j]);
      j++;
    }
  }
  j = 0;
  for (int i = 8; i < 2*4*(difficulty+2); i += 8){
    mvwvline(_gameBoard, 1, i, ACS_VLINE, 4*(difficulty+2)-1);
    if(j<difficulty){
      mvwprintw(_gameBoard,4*(difficulty+2)-1, i+6, "%d", _speeds[1][j]);
      mvwprintw(_gameBoard, 3, i+6, "%d", _speeds[0][j]);
      j++;
    }
  }
  wborder(_gameBoard, 0, 0, 0, 0, 0, 0, 0, 0);
  wrefresh(_gameBoard);

  _mess = newwin(1,8,1,131);
  mvwaddstr(_mess,0,0, "Messages");
  wrefresh(_mess);

  _messages = newwin(20, 90, 2, 90);
  scrollok(_messages, TRUE);
  wrefresh(_messages);

  _score = newwin(4, 20, 3*LINES/4,-10 + 3*COLS/4);
  wattrset(_score,COLOR_PAIR(3));
  for (int k = 0; k < 4; k++){
    for (int l = 0; l < 20; l++){
      waddch(_score, ' ');
    }
  }
  wborder(_score, 0, 0, 0, 0, 0, 0, 0, 0);
  mvwaddstr(_score, 0, 8, "Score");
  wattron(_score, COLOR_PAIR(1));
  mvwaddstr(_score, 1, 1, "Joueur 1");
  wattroff(_score, COLOR_PAIR(1));
  wattron(_score, COLOR_PAIR(2));
  mvwaddstr(_score, 1, 11, "Joueur 2");
  wattroff(_score, COLOR_PAIR(2));
  wattron(_score, COLOR_PAIR(3));
  mvwprintw(_score, 2, 5, "%d", Score0);
  mvwprintw(_score, 2, 14, "%d", Score1);
  wattroff(_score, COLOR_PAIR(3));
  wrefresh(_score);
}

void CurseInterface::update(){

}


int CurseInterface::show(){ // Fonction principale de l'interface
  wmove(_messages, 1, 0);
  int player;
  wchar_t pion;
  int pionInt;
  bool validAction = TRUE;

  while(!_p_core->gameEnded()){
    validAction = TRUE;
    while(_p_core->fetchAction()){wrefresh(_gameBoard);} //On exécute toutes les actions en attente
    if(_p_core->gameEnded()){
      break;
    }
    player = _p_core->getNextPlayer();
    displayBoard();
    wrefresh(_gameBoard);

    waddstr(_messages, "C'est au tour du joueur ");
    wprintw(_messages, "%d", player+1);
    waddch(_messages, '\n');

    pion = wgetch(_messages);
    switch (pion) {
      case '1' :
      case 'a' :
        pionInt = 1;
        break;
      case '2' :
      case 'z' :
        pionInt = 2;
        break;
      case '3' :
      case 'e' :
        pionInt = 3;
        break;
      case '4' :
      case 'r' :
        pionInt = 4;
        break;
      case '5' :
      case 't' :
        pionInt = 5;
        break;
      case '6' :
      case 'y' :
        pionInt = 6;
        break;
      case '7' :
      case 'u' :
        pionInt = 7;
        break;
      case '8' :
      case 'i' :
        pionInt = 8;
        break;
      case '9' :
      case 'o' :
        pionInt = 9;
        break;
      case 'q' :
      case 'Q' :
        return 0;
      default:
        validAction = FALSE;
    }
    if (not validAction){continue;}
    waddstr(_messages, "Le joueur ");
    wprintw(_messages, "%d", player+1);
    waddstr(_messages, " joue son pion ");
    wprintw(_messages, "%d", pionInt);
    waddch(_messages, '\n');
    waddch(_messages, '\n');

    _p_core->pushAction(player,pionInt);
    wrefresh(_gameBoard);
    wrefresh(_messages);

    Score0 = (int)_p_core->getScore(0);
    Score1 = (int)_p_core->getScore(1);
    wattron(_score,COLOR_PAIR(3));
    mvwprintw(_score, 2, 5, "%d", Score0);
    mvwprintw(_score, 2, 14, "%d", Score1);
    wattron(_score,COLOR_PAIR(3));
    wrefresh(_score);
  }
  waddch(_messages, '\n');
  waddstr(_messages,"Appuyez sur une touche pour fermer");
  getch();
  return 0;
}


void CurseInterface::displayBoard(){
  std::vector<std::vector<CellState>> const& board = _p_core->getBoard();
  for (size_t i = 0; i < board.size(); i++) {
    for (size_t j = 0; j < board.size(); j++) {
      if (board[i][j] == CellState::Right or board[i][j] == CellState::Left){
        wattron(_gameBoard, COLOR_PAIR(1));
        mvwaddch(_gameBoard, 4*i + 2, 8*j + 4, (char)board[i][j]);
        wattroff(_gameBoard, COLOR_PAIR(1));
      } else if (board[i][j] == CellState::Up or board[i][j] == CellState::Down){
        wattron(_gameBoard, COLOR_PAIR(2));
        mvwaddch(_gameBoard, 4*i + 2, 8*j + 4, (char)board[i][j]);
        wattroff(_gameBoard, COLOR_PAIR(2));
      } else {
        wattron(_gameBoard, COLOR_PAIR(3));
        mvwaddch(_gameBoard, 4*i + 2, 8*j + 4, (char)board[i][j]);
      }
    }
  }
}


void CurseInterface::message(GameMessage sampleMessage){
  switch (sampleMessage){
    case GameMessage::InvalidMove :
      waddstr(_messages,"Mouvement Invalide");
      waddch(_messages,'\n');
      wrefresh(_messages);
      break;
    case GameMessage::Victory0 :
      refresh();
      move(LINES/2,COLS/2-10);
      addstr("Victoire du joueur 1");
      break;
    case GameMessage::Victory1 :
      refresh();
      move(LINES/2,COLS/2-10);
      addstr("Victoire du joueur 2");
      break;
    default :
      break;
  }
}

CurseInterface::~CurseInterface(){
  endwin();
}
