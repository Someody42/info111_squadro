#include "SFMLInterface.hpp"

SFMLInterface::SFMLInterface(GameCore* p_core) : AbstractInterface(p_core), sf::RenderWindow(sf::VideoMode(sf::VideoMode::getDesktopMode().width*9./10,sf::VideoMode::getDesktopMode().height*9./10),
                                                                                            "Squadro", sf::Style::Titlebar | sf::Style::Close) {
  setFramerateLimit(10);
  _size = getSize().y;
  _sideWidth = getSize().x-10-_size;
  sf::Clock _currentMessageClock;
  std::queue<std::wstring> _messageQueue;

  if (!_font.loadFromFile("lmroman10-regular.otf"))
  {
    throw("Erreur : police inexistante");
  }
  _strongColors = {sf::Color(255,255,0,255),sf::Color(255,0,0,255)};
  _weakColors = {sf::Color(255,255,0,63),sf::Color(255,0,0,63)};

  _separation = sf::RectangleShape(sf::Vector2f(10.f,(float)_size));
  _separation.setPosition(_size,0);
  _separation.setFillColor(sf::Color::Black);

  _scoreZones = std::vector<sf::RectangleShape>(2,sf::RectangleShape(sf::Vector2f(.5*(getSize().x-10-_size),200)));
  _scoreZones[0].setPosition(_size+10,0);
  _scoreZones[1].setPosition(_size+10+_scoreZones[0].getSize().x,0);
  _scoreZones[0].setFillColor(_weakColors[0]);
  _scoreZones[1].setFillColor(_weakColors[1]);
  _scoreZones[0].setOutlineColor(_strongColors[0]);
  _scoreZones[1].setOutlineColor(_strongColors[1]);
  _scoreZones[0].setOutlineThickness(-10);
  _scoreZones[1].setOutlineThickness(-10);

  _playerNames = std::vector<sf::Text>(2);
  for (size_t i = 0; i < 2; i++) {
    _playerNames[i].setFont(_font);
    _playerNames[i].setCharacterSize(48);
    _playerNames[i].setFillColor(sf::Color::Black);
    _playerNames[i].setString("Joueur " + std::to_string(i+1));
    _playerNames[i].setPosition(_size+10+i*_sideWidth/2.+10,10);
  }

  _messageZone.setFont(_font);
  _messageZone.setCharacterSize(24);
  _messageZone.setFillColor(sf::Color::Black);
  _messageZone.setPosition(_size+10,200);

  std::vector<std::vector<CellState>> const& board = _p_core->getBoard();

  _numbers = std::vector<sf::Text>(4*(int)_p_core->getDifficulty());
  for (size_t i = 0; i < (unsigned int)_p_core->getDifficulty(); i++) {
    _numbers[4*i].setFont(_font);
    _numbers[4*i].setString(std::to_string(_p_core->getSpeeds()[0][i]));
    _numbers[4*i].setCharacterSize(24);
    _numbers[4*i].setFillColor(sf::Color::Black);
    _numbers[4*i].setPosition(10+0,(i+1)*_size*1./board.size());

    _numbers[4*i+1].setFont(_font);
    _numbers[4*i+1].setString(std::to_string(_p_core->getSpeeds()[1][i]));
    _numbers[4*i+1].setCharacterSize(24);
    _numbers[4*i+1].setFillColor(sf::Color::Black);
    _numbers[4*i+1].setPosition(10+_size*(board.size()-1.)/board.size(),(i+1)*_size*1./board.size());

    _numbers[4*i+2].setFont(_font);
    _numbers[4*i+2].setString(std::to_string(_p_core->getSpeeds()[1][i]));
    _numbers[4*i+2].setCharacterSize(24);
    _numbers[4*i+2].setFillColor(sf::Color::Black);
    _numbers[4*i+2].setPosition(10+(i+1)*_size*1./board.size(),_size*(board.size()-1.)/board.size());

    _numbers[4*i+3].setFont(_font);
    _numbers[4*i+3].setString(std::to_string(_p_core->getSpeeds()[0][i]));
    _numbers[4*i+3].setCharacterSize(24);
    _numbers[4*i+3].setFillColor(sf::Color::Black);
    _numbers[4*i+3].setPosition(10+(i+1)*_size*1./board.size(),0);
  }
}

int SFMLInterface::show() {
  std::vector<std::vector<CellState>> const& board = _p_core->getBoard();

  bool actionsRemaining = false;

  sf::Vector2i clickedCell(0,0);

  CellState clickedCellState;

  //Création du VertexArray pour la grille
  _grid = sf::VertexArray(sf::Lines, 4*(board.size()-1));
  for (size_t i = 0; i < board.size()-1; i++) {
    _grid[2*i].position = sf::Vector2f(0,(i+1)*_size*1./board.size());
    _grid[2*i].color = sf::Color::Black;
    _grid[2*i+1].position = sf::Vector2f(_size,(i+1)*_size*1./board.size());
    _grid[2*i+1].color = sf::Color::Black;
    _grid[2*(board.size()-1)+2*i].position = sf::Vector2f((i+1)*_size*1./board.size(),0);
    _grid[2*(board.size()-1)+2*i].color = sf::Color::Black;
    _grid[2*(board.size()-1)+2*i+1].position = sf::Vector2f((i+1)*_size*1./board.size(),_size);
    _grid[2*(board.size()-1)+2*i+1].color = sf::Color::Black;
  }

  //Création de la forme de base des pions
  _baseTriangle.setPointCount(3);
  _baseTriangle.setPoint(0,sf::Vector2f(0,-(_size*1./(2*board.size())-25)));
  _baseTriangle.setPoint(1,sf::Vector2f((_size*1./(2*board.size())-25),(_size*1./(2*board.size())-25)));
  _baseTriangle.setPoint(2,sf::Vector2f(-(_size*1./(2*board.size())-25),(_size*1./(2*board.size())-25)));

  _scoreTrianglesRadius = (_sideWidth/2-20)/(2.*_p_core->getPointsToWin());
  _scoreTriangles = std::vector<std::vector<sf::CircleShape>>(2,std::vector<sf::CircleShape>(_p_core->getPointsToWin(),sf::CircleShape(_scoreTrianglesRadius,3)));
  for (size_t i = 0; i < (size_t)_p_core->getPointsToWin(); i++) {
    _scoreTriangles[0][i].setFillColor(sf::Color::Black);
    _scoreTriangles[1][i].setFillColor(sf::Color::Black);
    _scoreTriangles[0][i].setPosition(_size+10+10+2*i*_scoreTrianglesRadius,100);
    _scoreTriangles[1][i].setPosition(_size+10+_sideWidth/2+10+2*i*_scoreTrianglesRadius,100);
  }
  update();
  /* ___BOUCLE PRINCIPALE___ */
  while(isOpen()){
    //Gestion des évènements
    sf::Event event;
    while(pollEvent(event)){
      switch (event.type) {
        case sf::Event::Closed:
          close();
          return 0;
        case sf::Event::MouseButtonPressed:
          if(event.mouseButton.button == sf::Mouse::Left && !actionsRemaining) {
            DEBUG(std::cout<<"Click !"<<std::endl;)
            clickedCell = (sf::Vector2i)((board.size()*1.f/_size)*(sf::Vector2f)sf::Mouse::getPosition(*this));
            clickedCellState = board[clickedCell.y][clickedCell.x]; // ATTENTION ! `board` est une liste de lignes ==> `board[y][x]`
            switch (clickedCellState) {
              case CellState::None:
                DEBUG(std::cout<<"Ya rien à bouger"<<std::endl;)
                break;
              case CellState::Left:
              case CellState::Right:
                if(_p_core->getAI(0)==nullptr)
                  _p_core->pushAction(0,clickedCell.y);
                break;
              case CellState::Up:
              case CellState::Down:
                if(_p_core->getAI(1)==nullptr)
                  _p_core->pushAction(1,clickedCell.x);
                break;
            }
          }
          break;
        case sf::Event::KeyPressed:
          if(event.key.code == sf::Keyboard::Escape){ //DANS TOUS LES CAS
            close();
            return 0;
          }
          if(!actionsRemaining && _p_core->getAI(_p_core->getNextPlayer())==nullptr){
            switch (event.key.code) {
              case sf::Keyboard::Key::Numpad1:
              case sf::Keyboard::Key::Num1:
                _p_core->pushAction(_p_core->getNextPlayer(),1);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              case sf::Keyboard::Key::Numpad2:
              case sf::Keyboard::Key::Num2:
                _p_core->pushAction(_p_core->getNextPlayer(),2);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              case sf::Keyboard::Key::Numpad3:
              case sf::Keyboard::Key::Num3:
                _p_core->pushAction(_p_core->getNextPlayer(),3);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              case sf::Keyboard::Key::Numpad4:
              case sf::Keyboard::Key::Num4:
              case sf::Keyboard::Key::Quote:
                _p_core->pushAction(_p_core->getNextPlayer(),4);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              case sf::Keyboard::Key::Numpad5:
              case sf::Keyboard::Key::Num5:
                _p_core->pushAction(_p_core->getNextPlayer(),5);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              case sf::Keyboard::Key::Numpad6:
              case sf::Keyboard::Key::Num6:
              case sf::Keyboard::Key::Dash:
                _p_core->pushAction(_p_core->getNextPlayer(),6);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              case sf::Keyboard::Key::Numpad7:
              case sf::Keyboard::Key::Num7:
                _p_core->pushAction(_p_core->getNextPlayer(),7);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              case sf::Keyboard::Key::Numpad8:
              case sf::Keyboard::Key::Num8:
                _p_core->pushAction(_p_core->getNextPlayer(),8);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              case sf::Keyboard::Key::Numpad9:
              case sf::Keyboard::Key::Num9:
                _p_core->pushAction(_p_core->getNextPlayer(),9);
                actionsRemaining = true; //On ne traitera pas de nouvelles actions pendant le même tour
                break;
              default:
              break;
            }
          }
          break;
        default:
          break;
      }
    }

    //Début de l'affichage : on clear tout
    clear(sf::Color::White);

    draw(_separation);

    if(_messageQueue.empty()) {
      _currentMessageClock.restart();
    }else if(_currentMessageClock.getElapsedTime()<=sf::seconds(1)) {
      _messageZone.setString(_messageQueue.front());
    }else{
      _messageZone.setString("");
      _messageQueue.pop();
      _currentMessageClock.restart();
    }

    draw(_messageZone);

    //Affichage de la grille
    draw(_grid);

    //Affichage des numéros
    for (size_t i = 0; i < _numbers.size(); i++) {
      draw(_numbers[i]);
    }

    for (size_t i = 0; i < _scoreZones.size(); i++) {
      draw(_scoreZones[i]);
    }

    for (size_t i = 0; i < _playerNames.size(); i++) {
      draw(_playerNames[i]);
    }

    for (size_t i = 0; i < _scoreTriangles.size(); i++) {
      for (size_t j = 0; j < _scoreTriangles[i].size(); j++) {
        draw(_scoreTriangles[i][j]);
      }
    }
    //Affichage des pions
    sf::ConvexShape triangle;
    for (size_t i = 0; i < board.size(); i++) {
      for (size_t j = 0; j < board.size(); j++) {
        triangle = _baseTriangle;
        switch (board[i][j]) {
            case CellState::Up:
            triangle.setFillColor(sf::Color::Red);
            triangle.setPosition((_size*1./(2.*board.size()))*(2*j+1),(_size*1./(2.*board.size()))*(2*i+1));
            draw(triangle);
            break;
          case CellState::Down:
            triangle.setFillColor(sf::Color::Red);
            triangle.rotate(M_PI*180/M_PI);
            triangle.setPosition((_size*1./(2.*board.size()))*(2*j+1),(_size*1./(2.*board.size()))*(2*i+1));
            draw(triangle);
            break;
          case CellState::Right:
            triangle.setFillColor(sf::Color::Yellow);
            triangle.rotate(M_PI/2*180/M_PI);
            triangle.setPosition((_size*1./(2.*board.size()))*(2*j+1),(_size*1./(2.*board.size()))*(2*i+1));
            draw(triangle);
            break;
          case CellState::Left:
            triangle.setFillColor(sf::Color::Yellow);
            triangle.rotate(-M_PI/2*180/M_PI);
            triangle.setPosition((_size*1./(2.*board.size()))*(2*j+1),(_size*1./(2.*board.size()))*(2*i+1));
            draw(triangle);
            break;
          default:
            break;
        }
      }
    }
    //Affichage final de la fenêtre
    display();
    actionsRemaining = _p_core->fetchAction();
  }
  return 0;
}

void SFMLInterface::update() {
  unsigned int score0 = _p_core->getScore(0);
  unsigned int score1 = _p_core->getScore(1);
  for (size_t i = 0; i < score0; i++) {
    _scoreTriangles[0][i].setFillColor(sf::Color::Yellow);
  }
  for (size_t i = 0; i < score1; i++) {
    _scoreTriangles[1][i].setFillColor(sf::Color::Red);
  }
  int nextPlayer = _p_core->getNextPlayer();
  if(!_p_core->gameEnded()){
    _scoreZones[nextPlayer].setOutlineThickness(-10);
    _scoreZones[1-nextPlayer].setOutlineThickness(0);
  }
}

SFMLInterface::~SFMLInterface() {

}

void SFMLInterface::message(GameMessage msg) {
  switch (msg) {
    case GameMessage::Victory0:
    case GameMessage::Victory1:
      while(!_messageQueue.empty()){
        _messageQueue.pop(); //On vide les messages en attentes
      }
      _messageZone.setString(GameCore::messageTexts[msg]);
      _messageZone.setCharacterSize(48);
      _scoreZones[(int)msg].setOutlineThickness(-10);
      _scoreZones[1-(int)msg].setOutlineThickness(-10);
      _scoreZones[1-(int)msg].setOutlineColor(sf::Color::Black);
      _scoreZones[1-(int)msg].setFillColor(sf::Color(127,127,127));
      break;
    default:
      message(GameCore::messageTexts[msg]);
      break;
  }
}

void SFMLInterface::message(std::wstring msg) {
  _messageQueue.push(msg);
}
