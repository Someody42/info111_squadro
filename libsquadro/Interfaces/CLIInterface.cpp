#include "CLIInterface.hpp"

/*Constructeur de CLIInterface
 *@param GameCore* p_core un pointeur vers le GameCore correspondant
 *Appelle le constructeur de AbstractInterface avec ce même paramètre
 */
CLIInterface::CLIInterface(GameCore* p_core, bool noDisp) : AbstractInterface(p_core), _playingPlayer(p_core->getNextPlayer()), _noDisp(noDisp) {

}

void CLIInterface::update() {
  DEBUG(std::cout << "Mise à jour de l'Interface" << std::endl;)
  if(_playingPlayer!=_p_core->getNextPlayer()){
    displayBoard();
    _playingPlayer=_p_core->getNextPlayer();
  }
}

int CLIInterface::show() {
  _playingPlayer = _p_core->getNextPlayer();
  int pion; // numéro de ligne ou de colonne du pion à jouer
  std::string buffer;
  DEBUG(std::cout << "C'est très l'affichage de l'interface"  << std::endl;)
  displayBoard();
  DEBUG(std::cout << "Début de la boucle de jeu" << std::endl;)
  while(!_p_core->gameEnded()){ //Boucle principale
    while(_p_core->fetchAction()){} //On exécute toutes les actions en attente
    if(_p_core->gameEnded()){
      break;
    }
    _playingPlayer = _p_core->getNextPlayer();
    std::cout << "C'est au joueur " << _playingPlayer+1 << " de jouer;" << std::endl << "Entrer le pion à déplacer ";
    while(true){
      std::cin >> buffer;
      if(tolower(buffer)=="q"){
        std::cout<<"==="<<std::endl;
        return 0;
      }
      try{
        pion = std::stoi(buffer); //Entrée du pion à déplacer
        break;
      }
      catch(...){
        std::cout << "Merci d'entrer un entier;" << std::endl;
        continue;
      }
    }
    _p_core->pushAction(_playingPlayer,pion);
  }
  displayBoard();
  return 0;
}

void CLIInterface::displayBoard() {
  if(_noDisp) return;
  std::vector<std::vector<CellState>> const& board = _p_core->getBoard();
  std::cout<<std::endl;
  for (size_t i = 0; i < board.size()-1; i++) {
    for (size_t j = 0; j < board.size()-1; j++) {
      std::cout<<" "<< (char) board[i][j]<<" |";
    }
    std::cout<<" "<< (char) board[i][board.size()-1];
    std::cout<<std::endl;
    for (size_t i = 0; i < board.size()-1; i++) {
      std::cout<<"---+";
    }
    std::cout<<"---"<<std::endl;
  }
  for (size_t j = 0; j < board.size()-1; j++) {
    std::cout<<" "<< (char) board[board.size()-1][j]<<" |";
  }
  std::cout<<" "<< (char) board[board.size()-1][board.size()-1];
  std::cout<<std::endl<<std::endl;
}

void CLIInterface::message(std::wstring msg){
  if(_noDisp) return;
  std::wcout<<msg<<std::endl;
}

void CLIInterface::message(GameMessage msg){
  message(GameCore::messageTexts[msg]);
}
