#ifndef SFLM_INTERFACE_HPP
#define SFLM_INTERFACE_HPP

#include <SFML/Graphics.hpp>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>

#include "../../macros.hpp"
#include "AbstractInterface.hpp"

/*Classe SFMLInterface    A (pour la classe)
 *Hérite de AbstractInterface
 *Hérite de sf::RenderWindow (i.e cette classe désigne à la fois l'interface et la fenêtre SFML)
 *Implémente une interface graphique pour Squadro en utilisant la librairie SFML
 */
class SFMLInterface : public AbstractInterface, sf::RenderWindow{
private:
  std::queue<std::wstring> _messageQueue;
  sf::Clock _currentMessageClock;
  int _size;
  int _sideWidth;

  std::vector<std::vector<sf::CircleShape>> _scoreTriangles;
  float _scoreTrianglesRadius;
  sf::Font _font;
  sf::VertexArray _grid;
  sf::ConvexShape _baseTriangle;
  sf::RectangleShape _separation;
  std::vector<sf::RectangleShape> _scoreZones;
  sf::Text _messageZone;
  std::vector<sf::Text> _numbers;
  std::vector<sf::Text> _playerNames;

  std::vector<sf::Color> _strongColors;
  std::vector<sf::Color> _weakColors;

public:
  /*Constructeur de SFMLInterface
   *@param GameCore* p_core : un pointeur vers le GameCore correspondant
   *Appelle le constructeur de AbstractInterface avec ce même paramètre
   *Appelle le constructeur de sf::RenderWindow pour créer une fenêtre de 90% de
   *l'écran, avec pour titre "Squadro"
   *Limite le framerate à 60 FPS
   *Complexité : O(difficulty)
   */
  SFMLInterface(GameCore* p_core);

  /*Méthode update
   *Surcharge de la méthode update de AbstractInterface
   *Méthode appelée après qu'un coup ait été joué
   *Mets à jour la partie droite de la fenêtre en fonction de l'évolution du jeu
   *Complexité : O(score0 + score1)
   */
  virtual void update();

  /*Méthode show
   *Surcharge la méthode show de AbstractInterface
   *Gère l'affichage du jeu
   *Elle consiste en une boucle de dessin/jeu, et inclut la gestion des évènements SFML pour ensuite transmettre
   *les actions des joueurs au GameCore
   *Complexité : O(nombreDeTours * (difficulty^2 + nombre d'event SFML + complexité de fetchAction))
   */
  virtual int show();

  /*Méthode message
   *Surcharge de la Méthode message(std::wstring msg) de AbstractInterface;
   *@param std::wstring msg : la chaîne de caractère à afficher
   *Ajoute ce message à la file de messages
   *Complexité : O(1)
   */
  virtual void message(std::wstring msg);

  /*Méthode message
   *Surcharge de la Méthode message(GameMessage msg) de AbstractInterface;
   *@param GameMessage msg : la chaîne de caractère à afficher
   *Si il s'agit d'une victoire, le joueur vainqueur est affiché et la file de
   *messages est vidée
   *Sinon, l'autre fonction message est appelée avec la chaine correspondante
   *Complexité : O(1) si pas de victoire
   *             O(_messageQueue.size()) si victoire
   */
  virtual void message(GameMessage msg);

  /*Destructeur de SFMLInterface
   */
  virtual ~SFMLInterface();
};

#endif
