#ifndef GAME_MANAGER_HPP
#define GAME_MANAGER_HPP

#include "../macros.hpp"
#include "Interfaces/AbstractInterface.hpp"
#include "Interfaces/CLIInterface.hpp"
#include "AIs/AbstractAI.hpp"

#include <cmath>
#include <iostream>
#include <vector>
#include <utility>
#include <thread>
#include <chrono>
#include <algorithm>
#include <queue>
#include <map>
#include <fstream>
#include <sstream>

/*Énumération GameDifficulty :
 *Décrit les différentes difficutés possibles
 *Chacune d'entre-elle est associée à un entier correspondant au nombre de pions de chaque joueur
 */
enum class GameDifficulty : unsigned int {Easy = 5, Normal = 7};

/*Énumération GameMessage :
 *Décrit les différents messages entre le GameCore et les Interfaces
 */
enum class GameMessage : unsigned int {WrongTurn = 2, InvalidMove = 3, Victory0 = 0, Victory1 = 1};

/*Énumération CellState :
 *Décrit tous les états dans lequel peut se trouver une case du plateau :
 *None désigne une case vide
 *Les autres valeurs correspondent respectivement à une case dans lequel se trouve une pièce,
 *et la valeur précise décrit l'orientation de la pièce
 */
enum class CellState : char {None = ' ', Up = '^', Down = 'v', Left = '<', Right = '>'};

/*Classe : GameCore   AJ
 *Gère l'aspect technique d'une partie : gestion des mouvements, de la sauvegarde...
 */
class GameCore{
private:
  /*Structure interne qui ermet de représenter les actions*/
  struct Action {
    int _player;
    int _pieceMoved;
    int _stepsRemaining;
    Action(int player, int pieceMoved);
  };

  int _turnCount;
  int _turnLimit;
  bool _hasTurnLimit;
  bool _draw;
  std::queue<int> _actionLog;
  std::queue<Action> _actionQueue;
  GameDifficulty _difficulty;
  std::vector<std::vector<int>> _speeds;
  std::vector<int> _scores;
  std::vector<AbstractAI*> _vp_AI; //Pointeurs sur les IAs. Si jeu sans IA, valent `nullptr`
  int _pointsToWin;
  int _nextPlayer;
  AbstractInterface* _p_interface;
  std::vector<std::vector<CellState>> _board; //le plateau de jeu
  std::string _savePath;
  std::string _logPath;
public:
  /*Constructeur par défaut de GameCore
   *Initialise le pointeur sur l'interface à 'nullptr'
   *Initialise le plateau de jeu, les vitesses et l'objectif de victoire
   *en fonction de la difficulté choisie
   *@param GameDifficulty difficulty : la difficulté du jeu
   *@param std::string savePath : le fichier dans lequel sauvegarder
   *@param std::string logPath : le fichier dans lequel faire les logs
   */
  GameCore(GameDifficulty difficulty = GameDifficulty::Easy, std::string savePath="squadro.save", std::string logPath="squadro.log", int turnLimit=-1);

  /*Constructeur de GameCore
  *Utilisé seulement pour simplifier les tests
  *@param std::vector<std::vector<CellState>> startingBoard : l'état initial du plateau
  *@param GameDifficulty difficulty : la difficulté du jeu
  *Complexité : O(startingBoard.size()^2)
  */
  GameCore(std::vector<std::vector<CellState>> startingBoard, GameDifficulty difficulty = GameDifficulty::Easy);

  /*Constructeur de copie de GameCore
   *Permet de crée une copie du GameCore afin de simuler des actions ou autres sans que celles-ci soit transmises à l'interface
   *@param GameCore const& other : un autre GameCore
   *Complexité : O(other.difficulty^2)
   */
  GameCore(GameCore const& other);

  /*Méthode linkInterface
   *Lie le GameCore à un objet d'une classe descendant de AbstractInterface
   *@param p_interface : un pointeur vers l'interface à lier
   *Complexité : O(1)
   */
  void linkInterface(AbstractInterface* p_interface);

  /*Méthode addAI
   *Lie le GameCore à un objet d'une classe descendant de AbstractAI
   *@param AbstractAI* p_AI : un pointeur sur l'IA
   *Complexité : O(1)
   */
  void addAI(AbstractAI* p_AI);

  /*Méthode play
   *Lance le jeu
   *Appelle p_interface->show()
   *@return int : 0 si tout s'est bien passé, un autre code sinon
   *Complexité : C_IA ou C_Affichage
   */
  int play();

  /*Méthode pushAction
   *Appelée par l'interface pour donner une action à effectuer au GameCore
   *Cette action est ensuite stockée dans un  buffer avant d'être éxécutée (voir fetchAction)
   *@param int player : le numéro du joueur qui a joué (0 ou 1)
   *@param int pieceMoved : le numéro de la pièce jouée
   *Complexité : O(1)
   */
  void pushAction(int player, int pieceMoved);

  /*Méthode fetchAction
   *Appelée par l'interface lorsqu'une action est faite
   *Vérifie que cette action est valide
   *Appelle la méthode update de l'interface (si l'action est valide) pour lui signaler de mettre
   *à jour son affichage
   *@return : true si il reste des actions à effectuer, false sinon
   *Complexité : O(difficulty^2) + C_IA + C_Update (+ _actionQueue.size() en fin de jeu)
   */
  bool fetchAction();

  /*Méthode simulateAction
   *Permet de simuler une Action sur le board sans modifier le board en lui même
   *@param int player le numéro du joueur qui effectue l'actions
   *@param int pieceMoved le numéro de la pièce jouée
   *@return GameCore simulatedGame, une copie du board suite à l'action simulée afin de le comparer avec le board réel
   *Complexité (au pire) : O(difficulty^3)
   */
  GameCore simulateAction(int player, int pieceMoved);

  /*Méthode gameEnded
   *Teste, en se basant sur les scores, si le jeu est fini
   *Envoie les messages de victoire à l'interface
   *@return true si un des joueurs q gagné, false sinon
   *Complexité : O(1)
   */
  bool gameEnded() const;

  /*Méthode save
   *Sauvegarde dans un fichier la partie en cours en utilisant le format demandé
   *Complexité : O(difficulty^2+difficulty) = O(difficulty^2) écriture fichier
   */
  void save();

  /*Méthode logLastAction
   *Enregistre la succession des mouvements éffectués par les joueurs
   *à la fois dans le fichier et dans le tableau _actionLog
   *Complexité : O(1)
   */
  void logLastAction();

  /*Méthode buildBoard
   *Construit le plateau en fonction du chemin passé en paramètre :
   * - si path=="" on génère le plateau par défaut
   * - si path!="" et le fichier existe et est au bon format, on génère le plateau correspondant
   * - si path!="" et il y a une erreur de lecture, jette unr erreur
   *@param std::string path : le chemin à charger
   *Complexité : O(difficulty^2)
   */
  void buildBoard(std::string path);

  /*Destructeur du GameCore
   */
  virtual ~GameCore() = default;

  /*Méthode isValid
   *Vérifie que le joueur ne cherche pas à jouer un pion qui a déjà marqué un point
   *@param int player, int pieceMoved, une Action (voir Structure);
   *@return true si l'action est valide, false si elle ne l'est pas
   *Complexité : O(1)
   */
  bool isValid(int player, int pieceMoved);

  /*Méthode flipBoard
   *Gère la rotation du plateau pour les AI et la Sauvegarde
   *@param std::vector<std::vector<CellState>>, le board actuel
   *@return std::vector<std::vector<CellState>>, le même board après symétrie par rapport à l'axe diagonal montant
   *Complexité : O(difficulty^2)
   */
  static std::vector<std::vector<CellState>> flipBoard(std::vector<std::vector<CellState>> const&);

  static std::map<GameMessage,std::wstring> messageTexts;
  static std::map<GameDifficulty,std::vector<std::vector<int>>> paramSpeeds;
  static std::map<GameDifficulty,int> paramPointsToWin;

/*___ACCESSEURS___*/
//compléxité de O(1) si non précisé

  /*Accesseur de _board
  *@return une référence constante sur le tableau représentant le plateau de jeu
  */
  std::vector<std::vector<CellState>> const& getBoard() const;

  /*Accesseur de _nextPlayer
   *@return une référence constante sur le numéro du prochain joueur à jouer
   */
  int const& getNextPlayer() const;

  /*Accesseur de _scores
    *@return le score du joueur entré en paramètre
    */
  int const& getScore(int i) const;

  /*Accesseur de _pointsToWin
   *@return le nombre de points nécessaires à la Victoire
   */
  int const& getPointsToWin() const;

  /*Accesseur du joueur qui a gagné
   *@return 0 si le score du joueur 0 est supérieur à celui du joueur 1, 1 sinon
   */
  int getWinner() const;

  /*Accesseur de _actionLog
   *@return un tableau contenant les pièces jouée successivement
   *Complexité : copie de queue = O(_actionLog.size())
   */
  std::queue<int> getActionLog() const;

  /*Accesseur de _speeds
   *@return un tableau contenant les vitesses
   *Complexité : copie de vector = O(difficulty)
   */
  std::vector<std::vector<int>> getSpeeds() const;

  /*Accesseur de _difficulty
   *@return la difficulté de la partie
   */
  GameDifficulty getDifficulty() const;

  std::string const& getSaveFile() const;

  /*Accesseur de _vp_AI
   *@param numéro du joueur
   *@return un pointeur sur l'IA qui lui est associée
   */
  AbstractAI const* getAI(int playerID) const;
};

#endif
