#include "GameCore.hpp"

std::map<GameMessage,std::wstring> GameCore::messageTexts = {{GameMessage::WrongTurn,L"Ce n'est pas votre tour !"},
                                                             {GameMessage::InvalidMove,L"Mouvement non valide"},
                                                             {GameMessage::Victory0,L"Victoire du joueur 1"},
                                                             {GameMessage::Victory1,L"Victoire du joueur 2"}};

std::map<GameDifficulty,std::vector<std::vector<int>>> GameCore::paramSpeeds = {{GameDifficulty::Easy,{{1,3,2,3,1},{3,1,2,1,3}}},
                                                                                {GameDifficulty::Normal,{{1,2,3,2,3,2,1},{3,2,1,2,1,2,3}}}};

std::map<GameDifficulty,int> GameCore::paramPointsToWin = {{GameDifficulty::Easy,4},{GameDifficulty::Normal,6}};

GameCore::GameCore(std::vector<std::vector<CellState>> startingBoard, GameDifficulty gd) : GameCore(gd,"",""){
  _board = startingBoard;
}

GameCore::GameCore(GameDifficulty difficulty, std::string savePath, std::string logPath, int turnLimit) {
  _turnLimit = turnLimit;
  _hasTurnLimit = !(_turnLimit==-1);
  _turnCount=0;
  _draw=false;
  _savePath = savePath;
  _logPath = logPath;
  _difficulty = difficulty;
  _p_interface = nullptr;
  _nextPlayer = 0;
  _scores = std::vector<int>(2,0);
  _vp_AI = std::vector<AbstractAI*>(2,nullptr);

  _speeds = paramSpeeds[_difficulty];
  _pointsToWin = paramPointsToWin[_difficulty];
  DEBUG(std::cout<<"Et le Game Manager fut" << std::endl;)
}

void GameCore::linkInterface(AbstractInterface* p_interface) {
  _p_interface = p_interface;
  DEBUG(std::cout << "BEEP BOOP : Connection effectuée" << std::endl;)
}

int GameCore::play() {
  DEBUG(std::cout << "Quel jeu... passionnant" << std::endl;)
  try{
    if(_p_interface!=nullptr){
      return _p_interface->show();
    }
    if(_vp_AI[0]!=nullptr){
      _vp_AI[0]->play(); //On lance le jeu des IA
    }
    return 0;
  }
  catch(...){
    return -1;
  }
}

void GameCore::pushAction(int player, int pieceMoved){
  _turnCount++;
  DEBUG(std::cout<<"Méthode pushAction"<<std::endl;)
  _actionQueue.push(Action(player,pieceMoved));
}

GameCore GameCore::simulateAction(int player, int pieceMoved){
  GameCore simulatedGame(*this);
  simulatedGame.pushAction(player,pieceMoved);
  while(simulatedGame.fetchAction()){}
  return simulatedGame;
}

bool GameCore::fetchAction() {
  //Complexité : Complexité de la méthode play de l'AI : C_IA
  if(_actionQueue.empty()){
    if(_vp_AI[_nextPlayer] != nullptr && !gameEnded()){
      _vp_AI[_nextPlayer]->play();
      return true;//Il faut encore traiter les mouvements de l'IA
    }
    return false;
  }
  int player = _actionQueue.front()._player;
  int pieceMoved = _actionQueue.front()._pieceMoved;
  int stepsRemaining = _actionQueue.front()._stepsRemaining;

  if(_hasTurnLimit && _turnCount>_turnLimit) {
    _draw = true;
  }

  DEBUG(std::cout << "Le joueur " << player << " joue sa pièce numéro " << pieceMoved << "(étapes restantes : " << stepsRemaining << ")" ";" << std::endl;)

  if(gameEnded()) { //Si le jeu est fini, complexité de O(_actionQueue.size())
    DEBUG(std::cout<<"Jeu fini"<<std::endl;)
    while (!_actionQueue.empty()) {
      _actionQueue.pop(); //On vide la file d'actions
    }
    return true;
  }
  if(player!=_nextPlayer) {
    if(_p_interface!=nullptr){
      _p_interface->message(GameMessage::WrongTurn);
    }
    _actionQueue.pop();
    return true;
  }
  if(pieceMoved<1 || pieceMoved>(int)_difficulty){
    if(_p_interface!=nullptr){
      _p_interface->message(GameMessage::InvalidMove);
    }
    _actionQueue.pop();
    return true;
  }
  //Plein de tests pour vérifier que l'action est valide
  bool moveDone = false; //variable pour vérifier que le pion effectue bien un seul mouvement
  //On exécute l'action si elle est valide
  std::vector<std::vector<CellState>> board = _board; //O(difficulty^2)
  if(player==1){
    pieceMoved=board.size()-1-pieceMoved;
    board = flipBoard(board); //O(difficulty^2)
  }

  unsigned int currentCell = std::find(board[pieceMoved].begin(),board[pieceMoved].end(),CellState::Right)-board[pieceMoved].begin(); //O(difficulty)
  if(currentCell<board.size() ) {
    DEBUG(std::cout<<"a"<<std::endl;)
    if(stepsRemaining==-1){
      stepsRemaining = _actionQueue.front()._stepsRemaining = _speeds[player][pieceMoved-1];
    }
    moveDone = true;
    if(board[pieceMoved][currentCell+1] == CellState::Up){
      board[board.size()-1][currentCell+1] = CellState::Up;
      DEBUG(std::cout << "Vous avez renvoyé une pièce à la case départ" << std::endl;)
      _actionQueue.front()._stepsRemaining=2;//2 car on enlève 1 à la fin
    } else if (board[pieceMoved][currentCell+1] == CellState::Down){
      board[0][currentCell+1] = CellState::Down;
      DEBUG(std::cout << "Vous avez renvoyé une pièce au checkpoint" << std::endl;)
      _actionQueue.front()._stepsRemaining=2;//2 car on enlève 1 à la fin
    }
    board[pieceMoved][currentCell] = CellState::None;
    board[pieceMoved][currentCell+1] = CellState::Right;
    if(currentCell+2 == board.size()){
      board[pieceMoved][currentCell+1] = CellState::Left;
      DEBUG(std::cout << "Le pion est au checkpoint" << std::endl;)
      stepsRemaining = _actionQueue.front()._stepsRemaining = 1;//On arrête l'action
    }
  }

  currentCell = std::find(board[pieceMoved].begin()+1,board[pieceMoved].end(),CellState::Left)-board[pieceMoved].begin(); //O(difficulty)
  if(currentCell <board.size() && !moveDone) {
    if(stepsRemaining==-1){
      stepsRemaining = _actionQueue.front()._stepsRemaining = _speeds[1-player][pieceMoved-1];
    }
    moveDone = true;
    if(board[pieceMoved][currentCell-1] == CellState::Up){
      board[board.size()-1][currentCell-1] = CellState::Up;
      DEBUG(std::cout << "Vous avez renvoyé une pièce à la case départ" << std::endl;)
      _actionQueue.front()._stepsRemaining=2;//2 car on enlève 1 à la fin
    } else if (board[pieceMoved][currentCell-1] == CellState::Down){
      board[0][currentCell-1] = CellState::Down;
      DEBUG(std::cout << "Vous avez renvoyé une pièce au checkpoint" << std::endl;)
      _actionQueue.front()._stepsRemaining=2;//2 car on enlève 1 à la fin
    }
    board[pieceMoved][currentCell] = CellState::None;
    board[pieceMoved][currentCell-1] = CellState::Left;
    if(currentCell == 1){
      board[pieceMoved][currentCell-1] = CellState::Left;
      DEBUG(std::cout << "Le pion marque un point" << std::endl;)
      _scores[player]++;
      stepsRemaining = _actionQueue.front()._stepsRemaining = 1;//On arrête l'action
    }
  }

  if(player==1){
    board = flipBoard(board);//O(difficulty^2)
  }
  _board = board;//O(difficulty^2)

  if(!moveDone){
    if(_p_interface!=nullptr){
      _p_interface->message(GameMessage::InvalidMove);
    }
    _actionQueue.front()._stepsRemaining=1;
  }

  //On affiche les scores
  DEBUG(std::cout << "Score0 = " << _scores[0] << "; Score1 = " << _scores[1] << std::endl;)
  //On change le prochain joueur si un mouvement a été effectué !
  _actionQueue.front()._stepsRemaining--;
  if(_actionQueue.front()._stepsRemaining==0){
    DEBUG(std::cout<<"Tour suivant !"<<std::endl;)
    if(moveDone){
      _nextPlayer = 1-_nextPlayer; //0 --> 1 et 1 --> 0
      logLastAction();
    }
    _actionQueue.pop();
    /*if(_vp_AI[_nextPlayer] != nullptr){
      _vp_AI[_nextPlayer]->play();//O(C_IA)
    }*/
    save();//O(difficulty^2)
  }
  if(gameEnded()) {
    if(_p_interface!=nullptr){
      _p_interface->message((_scores[0] > _scores[1]) ? GameMessage::Victory0 : GameMessage::Victory1);
    }
    if(_savePath != "") {
      std::ofstream file(_savePath);
      file.close();//On vide le contenu du fichier
    }
    while (!_actionQueue.empty()) { //O(_actionQueue.size())
      _actionQueue.pop(); //On vide la file d'actions
    }
  }

  //On update l'interface
  if(_p_interface!=nullptr){
    _p_interface->update(); //Complexité de l'update : C_Update
  }

  return true;
}

bool GameCore::isValid(int player, int pieceMoved) {
  return (player==0) ? (_board[pieceMoved][0]!=CellState::Left) : (_board[_board.size()-1][pieceMoved]!=CellState::Down);
}

bool GameCore::gameEnded() const {
  return _draw || _scores[0]>=_pointsToWin || _scores[1]>=_pointsToWin;
}

void GameCore::logLastAction(){
  _actionLog.push(_actionQueue.front()._pieceMoved);
  if(_logPath==""){
    return;
  }
  try{
    std::ofstream logFile(_logPath,std::ios::app);
    logFile << ((_actionQueue.front()._player==0) ? _actionQueue.front()._pieceMoved : ((int)_difficulty+1-_actionQueue.front()._pieceMoved)) << ",";
    logFile.close();
  }catch(...){
    return;
  }
}

void GameCore::addAI(AbstractAI* p_AI) {
  if (p_AI->getPlayerID() == 0) {
    _vp_AI[0] = p_AI;
  } else {
    _vp_AI[1] = p_AI;
  }
}

void GameCore::buildBoard(std::string path) {
  if(path==""){
    _board = std::vector<std::vector<CellState>>((int) _difficulty + 2,std::vector<CellState>((int) _difficulty + 2,CellState::None));
    for (size_t i = 0; i < (size_t) _difficulty; i++) {
      _board[i+1][0]=CellState::Right;
      _board[_board.size()-1][i+1]=CellState::Up;
    }
    if(_logPath!=""){
      std::ofstream logFile(_logPath); //Si on commences une nouvelle partie, on écrase les logs
      logFile.close();
    }
  }else{
    std::ifstream file;
    file.open(path);
    std::vector<CellState> flatBoard;
    std::string speedsLine;
    std::istringstream speedsStream;
    char cell;
    getline(file, speedsLine);
    if(speedsLine==""){
      throw("Unsupported game format");
    }
    std::vector<int> speeds;
    int speed;
    speedsStream.str(speedsLine);
    while(speedsStream>>speed){
      speeds.push_back(speed);
    }
    _difficulty = (GameDifficulty) speeds.size();
    _nextPlayer = std::find(paramSpeeds[_difficulty].begin(),paramSpeeds[_difficulty].end(),speeds)-paramSpeeds[_difficulty].begin();
    if(_nextPlayer==2){ //i.e ça ne correspond à rien de connu
      throw("Unsupported game format");
    }
    while(file>>cell){
      flatBoard.push_back((cell=='x' || cell=='.') ? CellState::None : (CellState) cell);
    }
    if(flatBoard.size()==0){
      throw("Empty save");
    }
    if(flatBoard.size() != std::pow((int)_difficulty+2,2)){
      throw("Non coherent shape");
    }
    file.close();
    _board = std::vector<std::vector<CellState>>((int) _difficulty + 2,std::vector<CellState>((int) _difficulty + 2,CellState::None));
    for (size_t i = 0; i < _board.size(); i++) {
      for (size_t j = 0; j < _board.size(); j++) {
        _board[i][j] = flatBoard[j+i*_board.size()];
      }
    }
    if(_nextPlayer==1){
      _board = flipBoard(_board);
    }
    for (size_t i = 1; i <= (int)_difficulty; i++) {
      _scores[0]+=(_board[i][0]==CellState::Left);
      _scores[1]+=(_board[_board.size()-1][i]==CellState::Down);
    }
  }
  _speeds = paramSpeeds[_difficulty];
  _pointsToWin = paramPointsToWin[_difficulty];
}

void GameCore::save() {
  auto board = _board;
  if(_nextPlayer==1){
    board = flipBoard(board);
  }
  CellState cell;
  if(_savePath == ""){
    return;
  }
  std::ofstream file(_savePath);
  file<<"  ";
  for (int speed : _speeds[_nextPlayer]) {
    file << speed << " ";
  }
  file << std::endl;
  for (size_t i = 0; i < board.size(); i++) {
    for (size_t j = 0; j < board.size(); j++) {
      cell = board[i][j];
      if(cell!=CellState::None){
        file<<(char)board[i][j]<<" ";
      }else{
        if((i==0 && j==0) || (i==board.size()-1 && j==0) || (i==0 && j==board.size()-1) || (i==board.size()-1 && j==board.size()-1)){
          file<<"x ";
        }else{
          file<<". ";
        }
      }
    }
    file<<std::endl;
  }
}

std::vector<std::vector<CellState>> GameCore::flipBoard(std::vector<std::vector<CellState>> const& board) {
  std::vector<std::vector<CellState>> ret(board);
  for (size_t j = 0; j < board.size(); j++) {
    for (size_t i = 0; i < board.size(); i++) {
      switch (board[i][j]) {
        case CellState::Right:
          ret[board.size()-1-j][board.size()-1-i]=CellState::Up;
          break;
        case CellState::Left:
          ret[board.size()-1-j][board.size()-1-i]=CellState::Down;
          break;
        case CellState::Up:
          ret[board.size()-1-j][board.size()-1-i]=CellState::Right;
          break;
        case CellState::Down:
          ret[board.size()-1-j][board.size()-1-i]=CellState::Left;
          break;
        default:
          ret[board.size()-1-j][board.size()-1-i]=CellState::None;
      }
    }
  }
  return ret;
}

std::vector<std::vector<CellState>> const& GameCore::getBoard() const{
  return _board;
}

int const& GameCore::getNextPlayer() const{
  return _nextPlayer;
}

int const& GameCore::getScore(int i) const{
  return _scores[i];
}

GameDifficulty GameCore::getDifficulty() const{
  return _difficulty;
}

std::vector<std::vector<int>> GameCore::getSpeeds() const {
  return _speeds;
}

AbstractAI const* GameCore::getAI(int playerID) const {
  return _vp_AI[playerID];
}

std::queue<int> GameCore::getActionLog() const {
  return _actionLog;
}

int GameCore::getWinner() const {
  if(!gameEnded()){
    return -1;
  }
  if(_draw){
    return 2;
  }
  return ((_scores[0] > _scores[1]) ? 0 : 1);
}

std::string const& GameCore::getSaveFile() const {
  return _savePath;
}

int const& GameCore::getPointsToWin() const {
  return _pointsToWin;
}

GameCore::GameCore(GameCore const& other) {
  _hasTurnLimit = false;
  _draw = false;
  _difficulty = other._difficulty;
  _speeds = other._speeds;
  _scores = other._scores;
  _p_interface = nullptr;
  _vp_AI = {nullptr,nullptr};
  _pointsToWin = other._pointsToWin;
  _nextPlayer = other._nextPlayer;
  _board = other._board;
  _savePath = _logPath = "";
}

/*___STRUCTURE ACTION___*/

GameCore::Action::Action(int player, int pieceMoved) {
  _player = player;
  _pieceMoved = pieceMoved;
  _stepsRemaining = -1;
}
