#ifndef ABSTRACT_AI_HPP
#define ABSTRACT_AI_HPP

class GameCore;

/*Classe AbstractAI   A
 *Classe abstraite décrivant les méthodes de base que doivent satisfairent les différentes AI
 */
class AbstractAI{
protected:
  GameCore* _p_core;
  int _playerID;
public:

  /*Constructeur de AbstractAI
   *@param GameCore* p_core un pointeur vers le GameCore correspondant
   *@param int playerID le numéro du joueur à qui attribuer AI
   *Appelle la méthode addAI pour signaler au GameCore qu'une AI a été ajoutée
   *Complexité : O(1)
   */
  AbstractAI(GameCore* p_core, int playerID=1);

  /*Méthode virtuelle pure play
   *S'occupe de choisir le mouvement effectué par l'AI
   *Doit appeler la méthode pushAction du GameCore pour transmettre son action
   */
  virtual void play() = 0;

  /*Destructeur de AbstractAI
   */
  virtual ~AbstractAI() = default;

  /*Méthode getPlayerID
   *Accesseur de _playerID
   *@retrun _playerID le numéro d'un joueur AI
   *Complexité : O(1)
   */
  int getPlayerID();
};

#include "../GameCore.hpp"

#endif
