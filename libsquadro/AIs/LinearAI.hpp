#ifndef LINEAR_AI_HPP
#define LINEAR_AI_HPP

#include "AbstractAI.hpp"
#include "../GameCore.hpp"
#include <ctime>
#include <cstdlib>

/*Classe LinearAI   J
 *Hérite de AbstractAI
 *Définit les mouvements simple d'une AI : ligne par ligne
 */
class LinearAI : public AbstractAI{
private:
  /* data */
public:
  /*Constructeur de LinearAI
   *@param GameCore* p_core un pointeur vers le GameCore correspondant
   *@param int playerID le numéro du joueur à qui attribuer AI
   *Appelle le constructeur de AbstractAI
   *Complexité : O(1)
   */
  LinearAI(GameCore* p_core, int playerID=1);

  /*Surcharge de la méthode play de AbstractAI
   *Fait avancer le pion n tant que c'est possible, puis passe au pion n+1
   *Complexité (au pire) : O(_p_core->getDifficulty())
   */
  virtual void play();

  /*Destructeur de LinearAI
   */
  virtual ~LinearAI() = default;
};

#endif
