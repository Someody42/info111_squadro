#ifndef SCORE_EVALUATORS_HPP
#define SCORE_EVALUATORS_HPP

#include <random>
#include <string>
#include <cmath>

#include "../GameCore.hpp"
#include "../utilities.hpp"

/*Classe abstraite AbstractScoreEvaluator
 *Décrit l'interface minimale à implémenter par un évaluateur de score
 */
class AbstractScoreEvaluator{
public:
  /*Méthode virtuelle pure evaluate
   *Évalue l'intérêt d'une position pour un joueur
   *@param GameCore* p_coreToEvaluate : un pointeur sur le GameCore représentant la
   *situation à évaluer
   *@param int player : le joueur pour lequel on veut mesurer l'intérêt de la position
   *@return float : le score de l'état représenté par p_coreToEvaluate pour le joueur player
   */
  virtual double evaluate(GameCore* p_coreToEvaluate, int player) = 0;

  /*Destructeur de AbstractScoreEvaluator
   */
  virtual ~AbstractScoreEvaluator() = default;
};

/*Classe AdvancementEvaluator   A
 *Implémente un moyen d'évaluer le score correspondant à une position en faisant la différence
 *des nombres de cases parcourues depuis la situation initiale
 */
class AdvancementEvaluator : public AbstractScoreEvaluator{
public:
  /*Méthode evaluate : surcharge de la méthode evaluate de AbstractScoreEvaluator
   *Évalue l'intérêt d'une position pour un joueur en comparant les avancements
   *@param GameCore* p_coreToEvaluate : un pointeur sur le GameCore représentant la
   *situation à évaluer
   *@param int player : le joueur pour lequel on veut mesurer l'intérêt de la position
   *@return float : le score de l'état représenté par p_coreToEvaluate pour le joueur player,
   *obtenu en faisantg la différence entre son avancement et celui de l'adversaire
   *Complexité : O(difficulty^2)
   */
  virtual double evaluate(GameCore* p_coreToEvaluate, int player);

  /*Méthode advancementOfPlayer
   *Calcule l'avancement d'un joueur
   *@param GameCore* p_coreToEvaluate : un pointeur sur le GameCore représentant la
   *situation à évaluer
   *@param int player : le joueur dont on veut calculer l'avancement
   *@return float :
   * - si on évalue un jeu non terminé, on renvoie le nombre de cases parcourues
   * - si le jeu est fini, on renvoir +infinity si on a gagné, -infinity sinon
   *Complexité : O(difficulty^2)
   */
  virtual double advancementOfPlayer(GameCore* p_coreToEvaluate, int player);

  /*Destructeur de AdvancementEvaluator
   */
  virtual ~AdvancementEvaluator() = default;
};

/*Classe MLPEvaluator
 *Hérite de AbstractScoreEvaluator
 *Implémente un moyen d'évaluer le score reposant sur un perceptron multi-couche
 *Ce perceptron est entrainé en jouant des parties au hasard, et le score cible
 *dépend alors de l'issue de la partie
 */
class MLPEvaluator : public AbstractScoreEvaluator{
private:
  GameDifficulty _difficulty;
  std::vector<int> _dims;
  std::vector<std::vector<std::vector<long double>>> _weights;
  std::vector<std::vector<long double>> _biases;
public:
  /*Méthode loadPreTrained
   *Charge les coefficients du perceptron depuis le fichier correspondant
   *Complexité au pire : O(taille du fichier ^3)
   */
  void loadPreTrained();

  /*Méthode save
   *Sauvegarde les fichiers du perceptron dans le fichier correspondant
   *Complexité au pire : O(dims.size()*max(dims)^2)
   */
  void save();

  /*Méthode feedAll
   *Méthode permettant au perceptron d'apprendre sur une partie entière, résumée
   *par les actions des joueurs et le gagnant
   *@param std::queue<int> actions : les actions des joueurs
   *@param int winner : l'ID du gagnant
   *Complexité : O(nombreDeCoups*dims.size()*max(dims)^2)
   */
  void feedAll(std::queue<int> actions, int winner);

  /*Méthode feedOne
   *Permet de faire une étape de backpropagation pour un état du jeu
   *@param GameCore* p_core : un pointeur sur le GameCore considéré
   *@param long double coeff : le coefficient de modification des coefficients
   *@param std::vector<std::vector<std::vector<long double>>> oldWeights :
   *les anciens poids, utilisés pour les prédictions
   *@param std::vector<std::vector<long double>> oldBiases :
   *les anciens biais, utilisés pour les prédictions
   *Complexité : O(dims.size()*max(dims)^2)
   */
  void feedOne(GameCore* p_core, int expected, long double coeff, std::vector<std::vector<std::vector<long double>>> oldWeights, std::vector<std::vector<long double>> oldBiases);

  /*Constructeur de MLPEvaluator
   *@param GameDifficulty difficulty : la difficulté du jeu
   *Complexité : O(dims.size()*max(dims)^2)*complexité d'un tirage aléatoire
   */
  MLPEvaluator(GameDifficulty difficulty);

  /*Méthode evaluate
   *Surcharge de la méthode evaluate de AbstractScoreEvaluator
   *Évalue à l'aide du réseau de neurones l'intérêt d'une position pour un joueur
   *@param int player : le joueur pour lequel on veut mesurer l'intérêt de la position
   *@return float : le score de l'état représenté par p_coreToEvaluate pour le joueur player,
   *obtenu en sortie du perceptron
   *Complexité : O(dims.size()*max(dims)^2)
   */
  virtual double evaluate(GameCore* p_coreToEvaluate, int player);

  /*Destructeur de MLPEvaluator
   */
  virtual ~MLPEvaluator() = default;

  /*Fonctions mathématiques de R dans R
   *Complexité supposée constante
   */
  static long double symetricSigmoid(long double x);
  static long double d_symetricSigmoid(long double x);
};

#endif
