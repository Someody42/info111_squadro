#include "RandomAI.hpp"

RandomAI::RandomAI(GameCore* p_core, int playerID) : AbstractAI(p_core, playerID){
  std::srand(std::time(nullptr));
}

void RandomAI::play(){
  int move;
  do {
    move = 1+std::rand()%((int)(_p_core->getDifficulty()));
  } while(!_p_core->isValid(_playerID,move));
  _p_core->pushAction(_playerID,move);
}
