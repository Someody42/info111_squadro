#include "ScoreMaximizingAI.hpp"

ScoreMaximizingAI::ScoreMaximizingAI(AbstractScoreEvaluator* p_scoreEvaluator, GameCore* p_core, int playerID) : AbstractAI(p_core,playerID) {
  _p_scoreEvaluator = p_scoreEvaluator;
}

void ScoreMaximizingAI::play() {
  std::vector<float> moves;
  moves.reserve((size_t)_p_core->getDifficulty());
  GameCore simulationResult;
  float score;
  bool bestMoveValid = false;
  int bestMove = 0;
  float bestScore;
  for (size_t i = 0; i < (size_t)_p_core->getDifficulty(); i++) {
    simulationResult = _p_core->simulateAction(_playerID,i+1);
    score=_p_scoreEvaluator->evaluate(&simulationResult,_playerID);
    moves.push_back(score);
  }
  for (size_t i = 0; i < (size_t)_p_core->getDifficulty(); i++) {
    if (!_p_core->isValid(_playerID,i+1)) {
      continue;
    }
    if(!bestMoveValid){
      bestMove = i+1;
      bestScore = moves[i];
      bestMoveValid = true;
    }else{
      if (moves[i]>bestScore) {
        bestMove = i+1;
        bestScore = moves[i];
      }
    }
  }
  _p_core->pushAction(_playerID,bestMove);
}

/*==*/

TreeExploringBestScoreMaximizingAI::TreeExploringBestScoreMaximizingAI(AbstractScoreEvaluator* p_scoreEvaluator, GameCore* p_core, int playerID, int depth) : ScoreMaximizingAI(p_scoreEvaluator, p_core, playerID), _depth(depth){

}

void TreeExploringBestScoreMaximizingAI::play(){
  double bestScore;
  int bestMove = 0;
  std::vector<double> scores((size_t)_p_core->getDifficulty());
  bool bestMoveValid = false;
  GameCore simulation(*_p_core);
  for (size_t i = 0; i < (size_t)_p_core->getDifficulty(); i++) {
    simulation = letAIPlayFrom(_p_core->simulateAction(_playerID,i+1),1-_playerID,_depth-1);
    scores[i] = _p_scoreEvaluator->evaluate(&simulation,_playerID);
  }
  for (size_t i = 0; i < (size_t)_p_core->getDifficulty(); i++) {
    if (!_p_core->isValid(_playerID,i+1)) {
      continue;
    }
    if(!bestMoveValid){
      bestMove = i+1;
      bestScore = scores[i];
      bestMoveValid = true;
    }else{
      if (scores[i]>bestScore) {
        bestMove = i+1;
        bestScore = scores[i];
      }
    }
  }
  _p_core->pushAction(_playerID,bestMove);
}

GameCore TreeExploringBestScoreMaximizingAI::letAIPlayFrom(GameCore core, int playerID, int depthRemaining) {
  if(core.gameEnded() || depthRemaining==0){
    return core;
  }
  double bestScore;
  std::vector<float> scores((size_t)_p_core->getDifficulty());
  GameCore ret(*_p_core);
  bool bestMoveValid = false;
  std::vector<GameCore> simulations((size_t)_p_core->getDifficulty());
  for (size_t i = 0; i < (size_t)_p_core->getDifficulty(); i++) {
    simulations[i] = letAIPlayFrom(core.simulateAction(playerID,i+1),1-playerID,depthRemaining-1);
    scores[i] = _p_scoreEvaluator->evaluate(&(simulations[i]),playerID);
  }
  for (size_t i = 0; i < (size_t)_p_core->getDifficulty(); i++) {
    if (!core.isValid(playerID,i+1)) {
      continue;
    }
    if(!bestMoveValid){
      ret = simulations[i];
      bestScore = scores[i];
      bestMoveValid = true;
    }else{
      if (scores[i]>bestScore) {
        ret = simulations[i];
        bestScore = scores[i];
      }
    }
  }
  return ret;
}
