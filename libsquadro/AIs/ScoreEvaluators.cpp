#include "ScoreEvaluators.hpp"

double AdvancementEvaluator::evaluate(GameCore* p_coreToEvaluate, int player) {
  return advancementOfPlayer(p_coreToEvaluate, player)-advancementOfPlayer(p_coreToEvaluate, 1-player);
}

double AdvancementEvaluator::advancementOfPlayer(GameCore* p_coreToEvaluate, int player) {
  int advancement=0;
  auto board = p_coreToEvaluate->getBoard();
  if(p_coreToEvaluate->gameEnded()){
    if(p_coreToEvaluate->getWinner() == player){
      return std::numeric_limits<double>::infinity();
    }else{
      return -std::numeric_limits<double>::infinity();
    }
  }
  board = (player==1) ? GameCore::flipBoard(board) : board;
  for (size_t i = 1; i <= (size_t)(p_coreToEvaluate->getDifficulty()); i++) {
    auto it_location = std::find(board[i].begin(),board[i].end(),CellState::Right);
    if(it_location!=board[i].end()){
      advancement += it_location-board[i].begin();
    }else{
      it_location = std::find(board[i].begin(),board[i].end(),CellState::Left);
      if(it_location!=board[i].end()){
        advancement += (int)(p_coreToEvaluate->getDifficulty()) + board[i].end()-it_location; //+1-1
      }
    }
  }
  return (double)advancement;
}

/*==*/

MLPEvaluator::MLPEvaluator(GameDifficulty difficulty) {
  std::cout<<"a"<<std::endl;
  _difficulty = difficulty;
  _dims = std::vector<int>({4*(2+(int)difficulty)*(2+(int)difficulty)+1,50,50,1});

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-1., 1.);

  auto random = [&dis, &gen](){
                                return dis(gen);
                              };

  std::vector<long double> tmpV;
  std::vector<std::vector<long double>> tmpM;
  for (size_t i = 1; i < _dims.size(); i++) {
    tmpV = std::vector<long double>(_dims[i]);
    std::generate(tmpV.begin(),tmpV.end(),random);
    _biases.push_back(std::vector<long double>(tmpV));
    tmpM = std::vector<std::vector<long double>>(_dims[i],std::vector<long double>(_dims[i-1]));
    for (size_t j = 0; j < (size_t)_dims[i]; j++) {
      std::generate(tmpM[j].begin(),tmpM[j].end(),random);
    }
    _weights.push_back(std::vector<std::vector<long double>>(tmpM));
  }
  loadPreTrained();
}

double MLPEvaluator::evaluate(GameCore* p_coreToEvaluate, int player) {
  auto board = p_coreToEvaluate->getBoard();
  if(4*board.size()*board.size()+1 != (size_t)_dims[0]){
    throw("Error : Difficulty has changed");
  }
  std::vector<long double> flatBoard(_dims[0],0);
  for (size_t i = 0; i < board.size(); i++) {
    for (size_t j = 0; j < board.size(); j++) {
      flatBoard[4*(i*board.size()+j)] = (board[i][j] == CellState::Right) ? 1 : 0;
      flatBoard[4*(i*board.size()+j)+1] = (board[i][j] == CellState::Left) ? 1 : 0;
      flatBoard[4*(i*board.size()+j)+2] = (board[i][j] == CellState::Up) ? 1 : 0;
      flatBoard[4*(i*board.size()+j)+3] = (board[i][j] == CellState::Down) ? 1 : 0;
    }
  }
  flatBoard[_dims[0]-1] = -1+2*p_coreToEvaluate->getNextPlayer();
  std::vector<long double> layer(flatBoard);
  for (size_t i = 0; i < _dims.size()-1; i++) {
    layer = matVProd(_weights[i],layer)+_biases[i];
    std::for_each(layer.begin(), layer.end(), [](long double & x){x = symetricSigmoid(x);});
  }
  return (-2*player+1)*layer[0];
}

long double MLPEvaluator::symetricSigmoid(long double x) {
  if(x>-500){
    return 1-2./(1+std::exp(x));
  }
  return -1+2./(1+std::exp(-x));
}

long double MLPEvaluator::d_symetricSigmoid(long double x) {
  return 2*std::exp(-x)/std::pow(1+std::exp(-x),2);
}

void MLPEvaluator::loadPreTrained(){
  std::setlocale(LC_NUMERIC, "C");
  std::string filename = "libsquadro/AIs/DataMLP/MLPData"+std::to_string((int)_difficulty);
  for (size_t i = 0; i < _dims.size(); i++) {
    filename += "-" + std::to_string(_dims[i]);
  }
  filename+=".data";

  std::string matrix;
  std::string vector;
  std::string component;

  std::ifstream file(filename);
  if(!file.is_open()){
    return;
  }
  for (size_t i = 0; i < _dims.size()-1; i++) {
    std::getline(file,matrix,'|');
    auto it_semicolon=matrix.begin();
    auto it_semicolonOld=it_semicolon;
    size_t j=0;
    while (it_semicolonOld!=matrix.end()) {
      vector = "";
      it_semicolon = std::find(it_semicolonOld,matrix.end(),';');
      std::copy(it_semicolonOld,it_semicolon,std::back_inserter(vector));
      auto it_comma=vector.begin();
      auto it_commaOld = it_comma;
      size_t k=0;
      while(it_commaOld!=vector.end()) {
        component = "";
        it_comma = std::find(it_commaOld,vector.end(),',');
        std::copy(it_commaOld,it_comma,std::back_inserter(component));
        if(component==""){break;}
        _weights[i][j][k]=std::stold(component);
        it_commaOld=it_comma+1;
        k++;
      }
      it_semicolonOld=it_semicolon+1;
      j++;
    }
    std::getline(file,vector,'|');
    auto it_comma=vector.begin();
    auto it_commaOld = it_comma;
    size_t k=0;
    while(it_commaOld!=vector.end()) {
      component = "";
      it_comma = std::find(it_commaOld,vector.end(),',');
      std::copy(it_commaOld,it_comma,std::back_inserter(component));
      if(component==""){break;}
      _biases[i][k]=std::stold(component);
      it_commaOld=it_comma+1;
      k++;
    }
  }
}

void MLPEvaluator::save(){
  std::string filename = "libsquadro/AIs/DataMLP/MLPData"+std::to_string((int)_difficulty);
  for (size_t i = 0; i < _dims.size(); i++) {
    filename += "-" + std::to_string(_dims[i]);
  }
  filename+=".data";

  std::ofstream file(filename);
  for (size_t i = 0; i < _dims.size()-1; i++) {
    for (size_t j = 0; j < _weights[i].size(); j++) {
      for (size_t k = 0; k < _weights[i][j].size(); k++) {
        file<<_weights[i][j][k]<<",";
      }
      file<<";";
    }
    file<<"|";
    for (size_t j = 0; j < _biases[i].size(); j++) {
      file<<_biases[i][j]<<",";
    }
    file<<"|";
  }
}

void MLPEvaluator::feedAll(std::queue<int> actions, int winner){
  std::vector<std::vector<std::vector<long double>>> oldWeights = _weights;
  std::vector<std::vector<long double>> oldBiases = _biases;
  int expected = -2*winner+1;
  GameCore* p_core = new GameCore(_difficulty);
  p_core->buildBoard("");
  int i = 0;
  while(!actions.empty()) {
    p_core->pushAction(i%2,actions.front());
    while(p_core->fetchAction()){}
    feedOne(p_core,expected,1./(100), oldWeights, oldBiases);
    i++;
    actions.pop();
  }
  delete p_core;
}

void MLPEvaluator::feedOne(GameCore* p_core, int expected, long double coeff, std::vector<std::vector<std::vector<long double>>> oldWeights, std::vector<std::vector<long double>> oldBiases){
  auto board = p_core->getBoard();
  std::vector<long double> flatBoard(_dims[0],0);
  for (size_t i = 0; i < board.size(); i++) {
    for (size_t j = 0; j < board.size(); j++) {
      flatBoard[4*(i*board.size()+j)+0] = (board[i][j] == CellState::Right) ? 1 : 0;
      flatBoard[4*(i*board.size()+j)+1] = (board[i][j] == CellState::Left) ? 1 : 0;
      flatBoard[4*(i*board.size()+j)+2] = (board[i][j] == CellState::Up) ? 1 : 0;
      flatBoard[4*(i*board.size()+j)+3] = (board[i][j] == CellState::Down) ? 1 : 0;
    }
  }
  flatBoard[_dims[0]-1] = p_core->getNextPlayer();
  std::vector<std::vector<long double>> layers;
  std::vector<std::vector<long double>> intermediateLayers;
  std::vector<long double> layer(flatBoard);
  layers.push_back(layer);
  for (size_t i = 0; i < _dims.size()-1; i++) {
    layer = matVProd(oldWeights[i],layer)+oldBiases[i];
    intermediateLayers.push_back(layer);
    std::for_each(layer.begin(), layer.end(), [](long double & x){x = symetricSigmoid(x);});
    layers.push_back(layer);
  }
  std::vector<long double> correction(std::vector<long double>({(long double)expected})-layer);
  std::vector<long double> nextCorrection = correction;

  int i=_dims.size()-2;
  while (i>=0) {
    std::vector<long double> sigmoidedCorrection = intermediateLayers[i];
    nextCorrection = std::vector<long double>(layers[i].size());
    std::for_each(sigmoidedCorrection.begin(), sigmoidedCorrection.end(), [](long double & x){x = d_symetricSigmoid(x);});
    sigmoidedCorrection = correction*sigmoidedCorrection;
    for (size_t k = 0; k < correction.size(); k++) {
      _biases[i][k] -= coeff*sigmoidedCorrection[k];
      _weights[i][k] -= coeff*sigmoidedCorrection[k]*layers[i];
      nextCorrection -= coeff*sigmoidedCorrection[k]*oldWeights[i][k];
    }
    correction = nextCorrection;
    i--;
  }
}
