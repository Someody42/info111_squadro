#include "AbstractAI.hpp"

AbstractAI::AbstractAI(GameCore* p_core, int playerID) {
  _p_core = p_core;
  _playerID = playerID;
  _p_core->addAI(this);
}

int AbstractAI::getPlayerID() {
  return _playerID;
}
