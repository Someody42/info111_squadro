#ifndef RANDOM_AI_HPP
#define RANDOM_AI_HPP

#include "AbstractAI.hpp"
#include "../GameCore.hpp"
#include <ctime>
#include <cstdlib>

/*Classe RandomAI   A
 *Hérite de AbstractAI
 *Définit une IA jouant aléatoirement un coup valide
 */
class RandomAI : public AbstractAI{
public:
  /*Constructeur de RandomAI
   *@param GameCore* p_core un pointeur vers le GameCore correspondant
   *@param int playerID le numéro du joueur à qui attribuer AI
   *Appelle le constructeur de AbstractAI
   */
  RandomAI(GameCore* p_core, int playerID=1);

  /*Surcharge de la méthode play de AbstractAI
   *Choisi un chiffre aléatoire n entre 1 et la difficulté tel qu'il soit valide de
   *bouger le pion n, et joue ce coup
   *Complexité au pire : O(infini) (et oui...)
   *Complexité en moyenne : dépend de la complexité d'un tirage aléatoire
   */
  virtual void play();

  /*Destructeur de RandomAI
   */
  virtual ~RandomAI() = default;
};

#endif
