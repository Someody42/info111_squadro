#ifndef SCORE_MAXIMIZING_AI_HPP
#define SCORE_MAXIMIZING_AI_HPP

#include "AbstractAI.hpp"
#include "ScoreEvaluators.hpp"

/*Classe ScoreMaximizingAI : Hérite de AbstractAI   A
 *Implémente une IA jouant les coups maximisant un score, évalué par un scoreEvaluator
 */
class ScoreMaximizingAI : public AbstractAI{
protected:
  AbstractScoreEvaluator* _p_scoreEvaluator;

public:
  /*Constructeur de ScoreMaximizingAI
   *@param AbstractScoreEvaluator* p_scoreEvaluator : un pointeur vers l'évaluateur à utiliser
   *@param GameCore* p_core : un pointeur vers le GameCore de la partie
   *@param int playerID : le numéro du joueur joué
   *Appelle le Constructeur de AbstractAI
   *Complexité : O(1)
   */
  ScoreMaximizingAI(AbstractScoreEvaluator* p_scoreEvaluator, GameCore* p_core, int playerID=1);

  /*Méthode play
   *Surcharge de la méthode play de AbstractAI
   *Teste toutes les possibilités d'action, et choisit l'action valide produisant
   *le plateau correspondant au meilleur score, évalué par _p_scoreEvaluator
   *Complexité : O(difficulty*complexité de evaluate)
   */
  virtual void play();

  /*Destructeur de ScoreMaximizingAI
   */
  virtual ~ScoreMaximizingAI() = default;
};

/*Classe TreeExploringBestScoreMaximizingAI : Hérite de ScoreMaximizingAI
 *Implémente une IA jouant les coups maximisant un score après _depth tours de jeu
 *Plus précisément : pour chaque coup possible dans l'immédiat, le score associé correspond
 *à l'évaluation du plateau situé _depth tours plus loin, en supposant que :
 * - au tour 1, le joueur adverse joue selon la même IA, de paramètre _depth-1
 * - au tour 2, ce joueur joue selon la même IA, de paramètre _depth-2
 *...
 *Au final, on obtient le même résultat que :
 * - construire tout l'arbre, et répéter en remontant vers la racine :
 * - remplacer chaque noeud au niveau n-1 par la meilleure branche pour le joueur jouant à ce tour
 */
class TreeExploringBestScoreMaximizingAI : public ScoreMaximizingAI{
private:
  int _depth;

public:
  /*Constructeur de TreeExploringBestScoreMaximizingAI
   *@param AbstractScoreEvaluator* p_scoreEvaluator : un pointeur vers l'évaluateur à utiliser
   *@param GameCore* p_core : un pointeur vers le GameCore de la partie
   *@param int playerID : le numéro du joueur joué
   *@param int depth : la profondeur de l'exploration
   *Appelle le constructeur de ScoreMaximizingAI
   *Complexité : O(1)
   */
  TreeExploringBestScoreMaximizingAI(AbstractScoreEvaluator* p_scoreEvaluator, GameCore* p_core, int playerID=1, int depth=2);

  /*Destructeur de TreeExploringBestScoreMaximizingAI
   */
  virtual ~TreeExploringBestScoreMaximizingAI() = default;

  /*Méthode play
   *Surcharge la méthode play de ScoreMaximizingAI
   *Construit les GameCore pour chaque mouvement possible, puis appelle letAIPlayFrom
   *pour voir leur évolution, et joue le mouvement dont la simulation est la plus avantageuse,
   *de la manière évaluée par _p_scoreEvaluator
   *Complexité au pire : O(difficulty^depth)
   */
  virtual void play();

  /*Méthode letAIPlayFrom
   *Similaire à play, mais renvoie le board complètement évolué, pour qu'il puisse
   *être utilisé
   *S'appelle récursivement
   *Construit les GameCore pour chaque mouvement possible, puis appelle letAIPlayFrom
   *en ayant diminué la profondeur restante de 1,
   *pour voir leur évolution, et ranvoie le board choisi, i.e celui avec le meilleur score
   *@param GameCore core : la situation initiale de la simulation
   *@param int remaining : la profondeur maximale restante
   *@param int playerID : l'ID du joueur pour qui maximiser le score
   *@return GameCore : le GameCore final jugé le plus avantageux
   *Complexité au pire : O(difficulty^remaining)
   */
  virtual GameCore letAIPlayFrom(GameCore core, int remaining, int playerID);
};

#endif
