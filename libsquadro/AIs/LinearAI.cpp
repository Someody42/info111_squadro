#include "LinearAI.hpp"

LinearAI::LinearAI(GameCore* p_core, int playerID) : AbstractAI(p_core, playerID){

}

void LinearAI::play(){
  for(int j = 1; j <= (int)_p_core->getDifficulty(); j++){
    if (_p_core->isValid(_playerID,j)) {
      _p_core->pushAction(_playerID,j);
      break;
    }
  }
}
