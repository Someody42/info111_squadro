#ifndef UTILITIES_HPP
#define UTILITIES_HPP

#include <vector>
#include <iostream>
#include <algorithm>
//AJ


/*fonction map_contains
 *permet de trouver si une entrée est présente dans une map
 *@param une map et l'entrée à chercher
 *@return true si l'entrée est présente, false sinon
 *Complexité (en moyenne) : O(log(map.size()))
 */
template <typename map_type>
bool map_contains(map_type map, typename map_type::key_type key){
  return (map.find(key)!=map.end());
}

/*
 *
 */
std::string tolower(std::string str);

//Définit le type Matrix
template <typename T>
using Matrix = std::vector<std::vector<T>>;

/*Opérateur *=
 *permet de faire le produit d'un vecteur par une constante
 *@param un vecteur et une constante à multiplier
 *@return le produit des deux
 *Complexité = O(u.size()) opérations
 */
template <typename T, typename U>
void operator*=(std::vector<T> & u, U a){
  std::for_each(u.begin(), u.end(), [a](T& x) {
                                                x*=a;
                                              });
}


/*Opérateur +=
 *permet de faire la somme de deux vecteurs de même taille
 *@param deux vecteurs à additionner
 *@return la somme des deux
 *Complexité : O(u.size()) opérations
 */
template <typename T>
void operator+=(std::vector<T> & u, std::vector<T> const& v){
  if(u.size()!=v.size()){
    throw("E");
  }
  for (size_t i = 0; i < u.size(); i++) {
    u[i]+=v[i];
  }
}

/*Opérateur *
 *permet de faire le produit terme à terme de deux vecteurs de même taille
 *@param deux vecteurs à multiplier
 *@return le produit terme à terme des deux
 *Complexité : O(u.size()) opérations
 */
template <typename T>
void operator*=(std::vector<T> & u, std::vector<T> const& v){
  if(u.size()!=v.size()){
    throw("E");
  }
  for (size_t i = 0; i < u.size(); i++) {
    u[i]*=v[i];
  }
}

//Les opérateurs suivant constitue en une simplification d'écriture des deux précédents (donc complexité identique)

/*Opérateur +
 *permet de faire la somme de deux vecteurs de même taille
 *@param deux vecteurs à additionner
 *@return la somme des deux
 */
template <typename T>
std::vector<T> operator+(std::vector<T> u, std::vector<T> const& v){
  u+=v;
  return u;
}

/*Opérateur *
 *permet de faire le produit terme à terme de deux vecteurs de même taille
 *@param deux vecteurs à multiplier
 *@return le produit terme à terme des deux
 */
template <typename T>
std::vector<T> operator*(std::vector<T> u, std::vector<T> const& v){
  u*=v;
  return u;
}

/*Opérateur *
 *permet de faire le produit d'un vecteur par une constante
 *@param un vecteur et la constante facteur à multiplier
 *@return le produit des deux
 */
template <typename T, typename U>
std::vector<T> operator*(std::vector<T> u, U a){
  u*=a;
  return u;
}

//Opérateur similaire
template <typename T, typename U>
std::vector<T> operator*(U a, std::vector<T> u){
  u*=a;
  return u;
}

/*Opérateur -1
 *calcule l'opposée d'un vecteur
 *@param un vecteur
 *@return son opposée (*(-1))
 */
template <typename T>
std::vector<T> operator-(std::vector<T> u){
  return (-1)*u;
}

/*Opérateur -=
 *permet de faire la somme d'un vecteur et de l'opposée d'un autre vecteur de même taille
 *@param deux vecteurs à additionner
 *@return la somme des deux
 *Complexité : O(2*u.size())
 */
template <typename T>
void operator-=(std::vector<T> & u, std::vector<T> const& v){
  u+=-v;
}

/*Opérateur -
 *permet de faire la soustraction de deux vecteurs de même taille
 *@param deux vecteurs à soustraire
 *@return la différence des deux
 */
template <typename T>
std::vector<T> operator-(std::vector<T> u, std::vector<T> const& v){
  u-=v;
  return u;
}

/*Fonction transpose
 *permet de calculer la tranposée d'un vector
 *@param un vector
 *@return sa transposée
 *Complexité : O(M.size()*M[0].size()) affectations
 */
template <typename T>
std::vector<std::vector<T>> transpose(std::vector<std::vector<T>> M){
  std::vector<std::vector<T>> ret(M[0].size(),std::vector<T>(M.size()));
  for (size_t i = 0; i < M.size(); i++) {
    for (size_t j = 0; j < M[0].size(); j++) {
      ret[j][i] = M[i][j];
    }
  }
  return ret;
}

/*Fonction matProd
 *définie le produit matriciel entre deux matrices
 *@param deux matrices A et B
 *@return leur produit matriciel
 *Complexité : O(A.size()*B[0].size()*A[0].size()) opérations
 */
template <typename T>
std::vector<std::vector<T>> matProd(std::vector<std::vector<T>> A, std::vector<std::vector<T>> B){
  if(A[0].size() != B.size()){
    throw("Uncompatible dimensions for internal law : matrix multiplication");
  }
  std::vector<std::vector<T>> ret(A.size(), std::vector<T>(B[0].size()));
  for (size_t i = 0; i < A.size(); i++) {
    for (size_t j = 0; j < B[0].size(); j++) {
      for (size_t k = 0; k < A[0].size(); k++) {
        ret[i][j] += A[i][k]*B[k][j];
      }
    }
  }
  return ret;
}

/*Fonction matVProd
 *définie le produit matriciel entre une matrice et un vector
 *@param une matrice A et un vector
 *@return leur produit matriciel
 *Complexité : O(A.size()*A[0].size()) opérations
 */
template <typename T>
std::vector<T> matVProd(std::vector<std::vector<T>> A, std::vector<T> u){
  if(A[0].size() != u.size()){
    throw("Uncompatible dimensions for internal law : matrix multiplication");
  }
  std::vector<T> ret(A.size());
  for (size_t i = 0; i < A.size(); i++) {
    for (size_t k = 0; k < A[0].size(); k++) {
      ret[i] += A[i][k]*u[k];
    }
  }
  return ret;
}

#endif
